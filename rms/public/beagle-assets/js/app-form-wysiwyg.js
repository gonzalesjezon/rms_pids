var App = (function() {
  'use strict';

  App.textEditors = function(elements) {

    $(elements).summernote({
      height: 150,
    });

  };

  return App;
})(App || {});
