<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Parameters
    |--------------------------------------------------------------------------
    |
    | Accessible via config('param.key');
    | eg: config('params._SUPER_ADMIN_ID_')
    | any other location as required by the application or its packages.
    */

    '_SUPER_ADMIN_ID_' => 1,
    'division' => [
        1 => 'Information Technology',
        2 => 'Human Resources',
        3 => 'Accounting',
    ],
    'employee_status' => [
        1 => 'Permanent',
        2 => 'Project',
        3 => 'Contract of Service'
    ],
    'nature_of_appointment' => [
        1 => 'Original',
        2 => 'Promotion',
    ],
    'publication' => [
        'agency' => 'Agency Web Site',
        'csc_bulletin' => 'CSC Bulletin of Vacant Position',
        'newspaper' => 'Newspaper',
        'others' => 'Others',
    ],
    'eligibility_type' => [
        1 => 'Career Service Professional',
        2 => 'Career Service Sub-professional',
        3 => 'RA 1080 (Bar Exams)',
        4 => 'RA 1080 (Board Exams)',
        5 => 'Certified Public Accountant Exams',
        6 => 'Career Executive Service Eligible',
        7 => 'Career Service Executive Eligible',

    ]
];
