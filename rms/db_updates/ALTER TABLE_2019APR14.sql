ALTER TABLE `applicants`
	ADD COLUMN `zip_code` VARCHAR(255) NULL DEFAULT NULL AFTER `gwa`;

ALTER TABLE `applicants`
	ADD COLUMN `permanent_zip_code` VARCHAR(255) NULL DEFAULT NULL AFTER `zip_code`;

ALTER TABLE `eligibilities`
	CHANGE COLUMN `name` `eligibility_ref` VARCHAR(225) NULL DEFAULT NULL AFTER `applicant_id`;