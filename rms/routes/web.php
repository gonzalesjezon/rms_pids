<?php

/**
 * Public Routes
 *
 */
Route::get('/frontend', 'FronEndController@index');
Route::resources(['frontend' => 'FronEndController']);
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/menu', 'HomeController@index')->name('menu');
Route::get('/careers', 'HomeController@careers')->name('careers');
Auth::routes();

/**
 * Routes that require user to be logged in
 * filtered by middleware auth in controller
 */
Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/non-plantilla', 'JobsController@nonPlantilla')->name('jobs.nonplantilla');
Route::get('evaluation/rating', 'EvaluationController@rating')->name('evaluation.rating');
Route::get('jobs/publish', 'JobsController@publish')->name('jobs.publish');
Route::get('evaluation/matrix-qualification', 'EvaluationController@matrixQualification')->name('evaluation.matrix');
Route::get('evaluation/comparative-ranking', 'EvaluationController@comparativeRanking')->name('evaluation.comparative');
Route::get('evaluation/report', 'EvaluationController@evaluationReport')->name('evaluation.report');
Route::get('evaluation/matrix-report', 'EvaluationController@matrixQualificationReport')->name('evaluation.matrix-report');
Route::get('evaluation/comparative-report', 'EvaluationController@comparativeReport')->name('evaluation.comparative-report');
Route::get('report/appointments_issued', 'ReportController@appointmentIssued')->name('report.appointments_issued');
Route::get('report/erasures_alteration', 'ReportController@erasureAlteration')->name('report.erasures_alteration');
Route::get('report/absence_qualified_eligible', 'ReportController@absenceQualifiedEligible')->name('report.absence_qualified_eligible');
Route::get('report/dibar', 'ReportController@dibarReport')->name('report.dibar');
Route::get('report/publication_vacant_position', 'ReportController@vacantPosition')->name('report.publication_vacant_position');
Route::get('report/resignation_acceptance', 'ReportController@resignationAcceptance')->name('report.resignation_acceptance');
Route::get('report/oath_office', 'ReportController@oathOffice')->name('report.oath_office');
Route::get('report/appointment_form_regulated', 'ReportController@appointmentFormRegulated')->name('report.appointment_form_regulated');
Route::get('report/medical_certificate', 'ReportController@medicalCertificate')->name('report.medical_certificate');
Route::get('report/preliminary_evaluation', 'ReportController@preliminaryEvaluation')->name('report.preliminary_evaluation');
Route::get('report/selection_lineup', 'ReportController@selectionLineup')->name('report.selection_lineup');
Route::get('report/checklist', 'ReportController@checklistReport')->name('report.checklist');
Route::get('appointment/report', 'AppointmentController@appointmentReport')->name('appointment.report');
Route::get('appointment/create-appointment', 'AppointmentController@createAppointmentForm')->name('appointment.create-appointment');
Route::get('appointment/report-form', 'AppointmentController@reportForm')->name('appointment.report-form');
Route::get('assumption/report', 'AssumptionController@assumptionReport')->name('assumption.report');
Route::get('preliminary_evaluation/getApplicant', 'PreliminaryEvaluationController@getApplicant')->name('evaluation.getapplicant');
Route::get('selected_applicant/selectedApplicant', 'SelectedApplicantController@selectedApplicant')->name('selected_applicant.selectedapplicant');

Route::post('applicant.store-qualified',
    'ApplicantController@storeQualified')->name('applicant.store-qualified');

Route::post('evaluation/store-matrix-qualification',
    'EvaluationController@storeMatrixQualification')->name('evaluation.storeMatrix');

Route::post('evaluation/store-comparative-ranking',
    'EvaluationController@storeComparativeRanking')->name('evaluation.storeComparative');

Route::post('appointment/store-appointment-form',
    'AppointmentController@storeAppointmentForm')->name('appointment.storeAppointmentForm');

Route::post('selected_applicant/store-appointee',
    'SelectedApplicantController@storeAppointee')->name('selected_applicant.storeAppointee');

Route::post('applicant/delete',
    'ApplicantController@delete')->name('applicant.delete');

Route::resources([
    'applicant' => 'ApplicantController',
    'config' => 'ConfigController',
    'preliminary_evaluation' => 'PreliminaryEvaluationController',
    'selected_applicant' => 'SelectedApplicantController',
    'evaluation' => 'EvaluationController',
    'jobs' => 'JobsController',
    'recommendation' => 'RecommendationController',
    'appointment' => 'AppointmentController',
    'appointment-form' => 'AppointmentFormController',
    'joboffer' => 'JobOfferController',
    'assumption' => 'AssumptionController',
    'attestation' => 'AttestationController',
    'report' => 'ReportController',
]);

