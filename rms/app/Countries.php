<?php
/**
 * Created by PhpStorm.
 * User: Lance
 * Date: 9/4/2018
 * Time: 8:54 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;


class Countries extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';
}