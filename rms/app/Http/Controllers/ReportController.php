<?php

namespace App\Http\Controllers;

use App\Job;
use App\PreliminaryEvaluation;
use App\Applicant;
use App\Recommendation;
use App\Appointment;
use App\AppointmentForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use DateTime;

class ReportController extends Controller
{
    /**
     * @var array list of available report documents
     */
    protected $reports = [
        'preliminary_evaluation' => 'Preliminary Evaluation',
        'selection_lineup' => 'Selection Line Up',
        'appointments_issued' => 'Appointments Issued',
        'checklist' => 'Checklist'
        // 'erasures_alteration' => 'CS Form No. 3 Certificate of  Erasures Alteration',
        // 'absence_qualified_eligible' => 'CS Form No. 5 Certification of the  Absence of a Qualified Eligible',
        // 'dibar' => 'CS Form No. 8 Report on DIBAR',
        // 'publication_vacant_position' => 'CS Form No. 9 Request for Publication of Vacant Positions',
        // 'resignation_acceptance' => 'CS Form No. 10 Acceptance of  Resignation',
        // 'oath_office' => 'CS Form No. 32 Oath of Office',
        // 'appointment_form_regulated' => 'CS Form No. 33-A Appointment Form - Regulated',
        // 'medical_certificate' => 'CS Form No. 211 Medical Certificate',
        // 'position_description' => 'DBM-CSC Form No. 1 Position Description Forms',
    ];

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Reports');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::all('id', 'title')->pluck('title', 'id');
        $applicants = DB::table('applicants')
            ->select(DB::raw('CONCAT(`first_name`, " " ,`last_name`) as fullname, id'))
            ->get()->pluck('fullname', 'id')->toArray();

        return view('report.index', [
            'reports' => $this->reports,
            'jobs' => $jobs,
            'applicants' => $applicants,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function appointmentIssued(Request $request){
        $date = new DateTime($request->date);
        $print_date = $date->format('F Y');

        $appointment = new AppointmentForm();

        return view('report.appointments_issued')->with([
            'print_date' => $print_date,
            'appointments' => $appointment->getModels()
        ]);
    }

    public function erasureAlteration(Request $request){
        return view('report.erasures_alteration');
    }

    public function absenceQualifiedEligible(Request $request){
        return view('report.absence_qualified_eligible');
    }

    public function dibarReport(Request $request){
        return view('report.dibar');
    }

    public function vacantPosition(Request $request){
        return view('report.publication_vacant_position');
    }

    public function resignationAcceptance(Request $request){
        return view('report.resignation_acceptance');
    }

    public function oathOffice(Request $request){
        return view('report.oath_office');
    }

    public function appointmentFormRegulated(Request $request){
        return view('report.appointment_form_regulated');
    }

    public function medicalCertificate(Request $request){
        return view('report.medical_certificate');
    }

    public function preliminaryEvaluation(Request $request){

        $applicant = Applicant::where('job_id',$request->id)->pluck('id')->toArray();
        $preliminary = PreliminaryEvaluation::whereIn('applicant_id',$applicant)->getModels();
        $jobs = Job::where('id',$request->id)->first();

        return view('report.preliminary_evaluation')
        ->with([
            'preliminary' => $preliminary,
            'jobs' => $jobs,

        ]);
    }

    public function selectionLineup(Request $request){

        $applicants = Applicant::where('job_id',$request->id)->pluck('id')->toArray();
        $job = Job::where('id',$request->id)->first();
        $recommend = Recommendation::whereIn('applicant_id',$applicants)->getModels();

        return view('report.selection_lineup')
        ->with([
            'recommend' => $recommend,
            'jobs' => $job
        ]);
    }

    public function checklistReport(Request $request){

        $appointment = new Appointment();
        $applicant = new Applicant();

        if ($request->id) {
            $applicant = Applicant::where('id', $request->id)
                ->first();
            $appointment = Appointment::where('applicant_id', $request->id)
                ->first();
        }

        return view('report.checklist')->with([
            'appointment' => $appointment,
            'applicant' => $applicant,
        ]);
    }
}
