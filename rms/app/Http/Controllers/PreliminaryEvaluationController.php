<?php

namespace App\Http\Controllers;

use App\PreliminaryEvaluation;
use App\Job;
use App\Applicant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;


class PreliminaryEvaluationController extends Controller
{
     /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Preliminary Evaluated Applicants');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $preliminary = PreliminaryEvaluation::latest()
            ->paginate($perPage);

        return view('preliminary_evaluation.index', [
            'preliminaryData' => $preliminary
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $jobs = Job::where('status', '=', 'plantilla')
            ->pluck('title', 'id')
            ->toArray();
        $preliminary = PreliminaryEvaluation::select('applicant_id')->get()->toArray();
        $applicants = Applicant::where('qualified',1)->whereNotIn('id',$preliminary)->getModels();

        return view('preliminary_evaluation.create')->with([
            'jobs' => $jobs,
            'applicants' => $applicants,
            'action' => 'PreliminaryEvaluationController@store',
            'actionQualified' => 'PreliminaryEvaluationController@getApplicant',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        foreach ($request->preliminary as $preliminaryData) {

            if(isset($preliminaryData['checked'])){

                $preliminary = PreliminaryEvaluation::where('applicant_id', $preliminaryData['applicant_id'])->first();
                if (empty($preliminary->id)) {
                    $preliminary = new PreliminaryEvaluation();
                }
                $preliminary->fill($preliminaryData);
                $preliminary->isc_chairperson = $request->isc_chairperson;
                $preliminary->isc_member_one = $request->isc_member_one;
                $preliminary->isc_member_two = $request->isc_member_two;
                $preliminary->ea_representative = $request->ea_representative;

                if ($preliminary->exists()) {
                    $preliminary->updated_by = Auth::id();
                } else {
                    $preliminary->created_by = Auth::id();
                }

                $preliminary->save();
            }

        }

        return redirect('/preliminary_evaluation')->with('success', 'The preliminary evaluation was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PreliminaryEvaluation  $preliminaryEvaluation
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreliminaryEvaluation  $preliminaryEvaluation
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreliminaryEvaluation  $preliminaryEvaluation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PreliminaryEvaluation $preliminaryEvaluation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreliminaryEvaluation  $preliminaryEvaluation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PreliminaryEvaluation::destroy($id);
        return redirect('/preliminary_evaluation')->with('success', 'Preliminary Evaluation data deleted!');
    }

    public function getApplicant(Request $request){

        $preliminary = new PreliminaryEvaluation;
        $preliminary = $preliminary->select('applicant_id')->get()->toArray();

        $currentJob = new Job();
        $applicant  = new Applicant;
        $applicants = [];
        if (!empty($request->all())) {
            $currentJob = Job::find($request->position_consideration);
            $applicants = $applicant
                ->where('job_id',$request->position_consideration)
                ->where('qualified',1)
                ->whereNotIn('id',@$preliminary)->getModels();
        }

        $jobs = Job::where('status', '=', 'plantilla')
            ->pluck('title', 'id')
            ->toArray();

        return view('preliminary_evaluation.create')->with([
            'action' => 'PreliminaryEvaluationController@store',
            'actionQualified' => 'PreliminaryEvaluationController@getApplicant',
            'applicants' => $applicants,
            'currentJob' => $currentJob,
            'jobs' => $jobs,
        ]);
    }
}
