<?php

namespace App\Http\Controllers;

use App\Applicant,
    App\Countries,
    App\Job,
    App\JobOffer,
    App\Education,
    App\Assumption,
    App\Attestation,
    App\Eligibility,
    App\WorkExperience,
    App\Training,
    Illuminate\Support\Facades\View,
    Auth,
    Illuminate\Http\Request;

class ApplicantController extends Controller
{


    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
        'first_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        // 'middle_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'last_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'email_address' => 'required|email'
    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Applicants');
        $this->middleware('auth', [
            'except' => ['create', 'store']
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 100;

        if (!empty($keyword)) {
            $applicants = Applicant::latest()->paginate($perPage);
        } else {
            $applicants = Applicant::latest()->paginate($perPage);
        }

        return view('applicant.index', compact('applicants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $applicant = new Applicant();
        $applicant->job_id = (null !== $request->id) ? $request->id : null;

        $countries = Countries::all('code', 'name')
            ->pluck('name', 'code')->toArray();
        $jobs = Job::where('publish',1)->getModels();

        if (!empty($applicant->job_id) && empty($jobs[$applicant->job_id])) {
            abort(404, 'Forbidden not found.');
        }

        $has_educ = $applicant->education->pluck('educ_level')->toArray();

        return view('applicant.create')->with([
            'action' => 'ApplicantController@store',
            'applicant' => $applicant,
            'civilStatus' => Applicant::getCivilStatus(),
            'gender' => Applicant::getGender(),
            'countries' => $countries,
            'jobs' => $jobs,
            'has_educ' => $has_educ,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->validationRules);

        $applicant = new Applicant;
        $applicant->fill($request->all());
        $applicant->reference_no = uniqid();
        $applicant->saveImageFileNames($request);
        $applicant->saveDocumentFileNames($request);
        $applicant->setIsFilipino($request->filipino);
        $applicant->setIsNaturalized($request->naturalized);
        $applicant->created_by = (\Auth::id()) ? \Auth::id() : 88888888;


        if ($applicant->save()) {
            $applicant->uploadImageFiles($request);
            $applicant->uploadDocumentFiles($request);
            $primary = $this->savePrimaryEduc($request->primary,$applicant->id);
            $secondary = $this->saveSecondaryEduc($request->secondary,$applicant->id);
            $vocational = $this->saveVocational($request->vocational,$applicant->id);
            $college = $this->saveCollege($request->college,$applicant->id);
            $graduate = $this->saveGraduate($request->graduate,$applicant->id);
            $eligibility = $this->saveEligibility($request->eligibility,$applicant->id);
            $workexperience = $this->saveWorkExperience($request->work_experience,$applicant->id);
            $training = $this->saveTraining($request->training,$applicant->id);
        }

        if (\Auth::check()) {
            return redirect('/applicant')->with('success', 'Successfully applied to vacant position.');
        } else {
            return redirect('/careers')->with('success', 'Successfully applied to vacant position.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        $applicant = Applicant::findOrFail($id);

        return view('applicant.show', [
            'applicant' => $applicant,
            'documentView' => $request->document
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $applicant = Applicant::findOrFail($id);
        // $education = Education::where('applicant_id',$id)->getModels();

        $countries = Countries::all('code', 'name')
            ->pluck('name', 'code')->toArray();

        $jobs = Job::first();

        $has_educ = $applicant->education->pluck('educ_level')->toArray();

        return view('applicant.edit')->with([
            'action' => 'ApplicantController@update',
            'applicant' => $applicant,
            'civilStatus' => Applicant::getCivilStatus(),
            'gender' => Applicant::getGender(),
            'countries' => $countries,
            'jobs' => $jobs,
            'has_educ' => $has_educ,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, $this->validationRules);

        $applicant = Applicant::findOrFail($id);
        $oldAttributes = $applicant->getAttributes();
        $applicant->fill($request->all());

        // update media names on db
        $applicant->saveImageFileNames($request, $oldAttributes);
        $applicant->saveDocumentFileNames($request, $oldAttributes);

        $applicant->setIsFilipino($request->filipino);
        $applicant->setIsNaturalized($request->naturalized);
        $applicant->updated_by = \Auth::id();

        if ($applicant->update()) {
            $applicant->deleteMediaFiles($request, $oldAttributes);
            $applicant->uploadImageFiles($request, $oldAttributes);
            $applicant->uploadDocumentFiles($request, $oldAttributes);
            $primary = $this->savePrimaryEduc($request->primary,$applicant->id);
            $secondary = $this->saveSecondaryEduc($request->secondary,$applicant->id);
            $vocational = $this->saveVocational($request->vocational,$applicant->id);
            $college = $this->saveCollege($request->college,$applicant->id);
            $graduate = $this->saveGraduate($request->graduate,$applicant->id);
            $eligibility = $this->saveEligibility($request->eligibility,$applicant->id);
            $workexperience = $this->saveWorkExperience($request->work_experience,$applicant->id);
            $training = $this->saveTraining($request->training,$applicant->id);

        }

        return redirect()->route('applicant.edit', ['id' => $applicant->id])->with('success',
            'Applicant post updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $applicant = Applicant::findOrFail($id);
        $applicant->deleteAllMediaFiles($applicant->getAttributes());
        Applicant::destroy($id);
        Education::where('applicant_id',$id)->delete();
        Eligibility::where('applicant_id',$id)->delete();
        WorkExperience::where('applicant_id',$id)->delete();
        Training::where('applicant_id',$id)->delete();

        return redirect('/applicant')->with('success', 'Applicant data deleted!');
    }

    public function saveEligibility($data,$applicant_id){

        foreach ($data as $key => $value) {

            if(isset($value['eligibility_ref'])){
                $eligibility = Eligibility::find(@$value['id']);

                if(empty($eligibility)){
                    $eligibility = new Eligibility;
                }
                $eligibility->applicant_id         = $applicant_id;
                $eligibility->eligibility_ref      = $value['eligibility_ref'];
                $eligibility->rating               = $value['rating'];
                $eligibility->exam_place           = $value['exam_place'];
                $eligibility->license_number       = $value['license_number'];
                $eligibility->license_validity     = $value['license_validity'];
                $eligibility->exam_date            = $value['exam_date'];
                $eligibility->save();
            }

        }
    }

    public function saveWorkExperience($data,$applicant_id){

        foreach ($data as $key => $value) {

            if(isset($value['position_title'])){
                $workexperience = WorkExperience::find(@$value['id']);

                if(empty($workexperience)){
                    $workexperience = new WorkExperience;
                }
                $workexperience->applicant_id           = $applicant_id;
                $workexperience->inclusive_date_from    = $value['inclusive_date_from'];
                $workexperience->inclusive_date_to      = $value['inclusive_date_to'];
                $workexperience->position_title         = $value['position_title'];
                $workexperience->department             = $value['department'];
                $workexperience->salary_grade           = $value['salary_grade'];
                $workexperience->status_of_appointment  = $value['status_of_appointment'];
                $workexperience->govt_service           = $value['govt_service'];
                $workexperience->save();
            }

        }
    }

    public function saveTraining($data,$applicant_id){

        foreach ($data as $key => $value) {

            if(isset($value['title_learning_programs'])){
                $training = Training::find(@$value['id']);

                if(empty($training)){
                    $training = new Training;
                }
                $training->applicant_id             = $applicant_id;
                $training->title_learning_programs  = $value['title_learning_programs'];
                $training->inclusive_date_from      = $value['inclusive_date_from'];
                $training->inclusive_date_to        = $value['inclusive_date_to'];
                $training->number_hours             = $value['number_hours'];
                $training->ld_type                  = $value['ld_type'];
                $training->sponsored_by             = $value['sponsored_by'];
                $training->save();
            }

        }
    }

    public function savePrimaryEduc($primary,$applicant_id){


        $education = Education::find(@$primary[0]['id']);
        if(empty($education)){
            $education = new Education;
        }

        if(isset($primary[0]['school_name'])){
            $education->applicant_id        = $applicant_id;
            $education->school_name         = $primary[0]['school_name'];
            $education->course              = $primary[0]['course'];
            $education->attendance_from     = $primary[0]['attendance_from'];
            $education->attendance_to       = $primary[0]['attendance_to'];
            $education->level               = $primary[0]['level'];
            $education->graduated           = $primary[0]['graduated'];
            $education->awards              = $primary[0]['awards'];
            $education->educ_level          = 1;
            $education->save();
        }

    }

    public function saveSecondaryEduc($secondary,$applicant_id){

        foreach ($secondary as $key => $value) {

            $education = Education::find(@$value['id']);
            if(empty($education)){
                $education = new Education;
            }
            if(isset($value['school_name'])){
                $education->applicant_id        = $applicant_id;
                $education->school_name         = $value['school_name'];
                $education->course              = $value['course'];
                $education->attendance_from     = $value['attendance_from'];
                $education->attendance_to       = $value['attendance_to'];
                $education->level               = $value['level'];
                $education->graduated           = $value['graduated'];
                $education->awards              = $value['awards'];
                $education->educ_level          = 2;
                $education->save();
            }
        }

    }

    public function saveVocational($vocational,$applicant_id){

        if($vocational){
            foreach ($vocational as $key => $value) {
                $education = Education::find(@$value['id']);
                if(empty($education)){
                    $education = new Education;
                }
                if(isset($value['school_name'])){
                    $education->applicant_id        = $applicant_id;
                    $education->school_name         = $value['school_name'];
                    $education->course              = $value['course'];
                    $education->attendance_from     = $value['attendance_from'];
                    $education->attendance_to       = $value['attendance_to'];
                    $education->level               = $value['level'];
                    $education->graduated           = $value['graduated'];
                    $education->awards              = $value['awards'];
                    $education->educ_level          = 3;
                    $education->save();
                }
            }
        }


    }

    public function saveCollege($college,$applicant_id){

        foreach ($college as $key => $value) {
            $education = Education::find(@$value['id']);
            if(empty($education)){
                $education = new Education;
            }

            if(isset($value['school_name'])){
                $education->applicant_id        = $applicant_id;
                $education->school_name         = $value['school_name'];
                $education->course              = $value['course'];
                $education->attendance_from     = $value['attendance_from'];
                $education->attendance_to       = $value['attendance_to'];
                $education->level               = $value['level'];
                $education->graduated           = $value['graduated'];
                $education->awards              = $value['awards'];
                $education->educ_level          = 4;
                $education->save();
            }
        }

    }

    public function saveGraduate($graduate,$applicant_id){

        if(isset($graduate)){
            foreach ($graduate as $key => $value) {
                $education = Education::find(@$value['id']);
                if(empty($education)){
                    $education = new Education;
                }
                if(isset($value['school_name'])){
                    $education->applicant_id        = $applicant_id;
                    $education->school_name         = $value['school_name'];
                    $education->course              = $value['course'];
                    $education->attendance_from     = $value['attendance_from'];
                    $education->attendance_to       = $value['attendance_to'];
                    $education->level               = $value['level'];
                    $education->graduated           = $value['graduated'];
                    $education->awards              = $value['awards'];
                    $education->educ_level          = 5;
                    $education->save();
                }
            }
        }


    }

    public function saveJobOffer($applicant_id){

        $joboffer = JobOffer::where('applicant_id',$applicant_id)->first();

        if(empty($joboffer)){
            $joboffer = new JobOffer;
        }

        $joboffer->applicant_id = $applicant_id;

        if($joboffer->exists()){
            $joboffer->updated_by = Auth::id();
        }else{
            $joboffer->created_by = Auth::id();
        }
        $joboffer->save();

    }

    public function saveAssumptionToDuty($applicant_id){

        $assumption = Assumption::where('applicant_id',$applicant_id)->first();

        if(empty($assumption)){
            $assumption = new Assumption;
        }

        $assumption->applicant_id = $applicant_id;

        if($assumption->exists()){
            $assumption->updated_by = Auth::id();
        }else{
            $assumption->created_by = Auth::id();
        }
        $assumption->save();

    }

    public function saveAttestation($applicant_id){

        $attestation = Attestation::where('applicant_id',$applicant_id)->first();

        if(empty($attestation)){
            $attestation = new Attestation;
        }

        $attestation->applicant_id = $applicant_id;

        if($attestation->exists()){
            $attestation->updated_by = Auth::id();
        }else{
            $attestation->created_by = Auth::id();
        }
        $attestation->save();

    }

    public function storeQualified(Request $request){

        $applicant = Applicant::find($request->applicant_id);
        $applicant->qualified = 1;
        $applicant->save();

        return redirect('/applicant')->with('success','Applicant updated successfully.');
    }

    public function delete(Request $request){

        switch ($request->level) {
            case 'education':
                Education::destroy($request->id);
                break;
            case 'eligibility':
                Eligibility::destroy($request->id);
                break;
            case 'experience':
                WorkExperience::destroy($request->id);
                break;
            case 'training':
                Training::destroy($request->id);
                break;
        }

        return json_encode(['status' => true ]);
    }
}
