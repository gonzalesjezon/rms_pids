<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests,
    App\Job,
    App\Office,
    App\Division,
    Illuminate\Http\Request,
    Illuminate\Support\Facades\View;
use http\Env\Response;
use Illuminate\Http\JsonResponse;

class JobsController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
        'title' => 'required|max:255',
        'annual_basic_salary' => 'regex:^[0-9]{1,2}([,.][0-9]{1,2})?$^'
    ];

    /**
     * Call behavior handling for authentication
     * authentication section via middleware
     * =====================================
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'careers']]);
        View::share('title', 'Job Posting');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $perPage = 100;
        $jobs = Job::latest()
            ->where('status', '=', 'plantilla')
            ->paginate($perPage);

        return view('jobs.index', [
            'jobs' => $jobs,
        ]);
    }

    /**
     * Display a listing of the resource [non-plantilla].
     *
     * @return \Illuminate\View\View
     */
    public function nonPlantilla(Request $request)
    {
        $perPage = 100;
        $jobs = Job::latest()
            ->where('status', '=', 'non-plantilla')
            ->paginate($perPage);

        return view('jobs.non-plantilla', [
            'jobs' => $jobs,
            'division' => config('params.division')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $request \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $job = new Job();
        if (empty($job->status)) {
            $job->status = $request->type;
        }
        $office = Office::pluck('name','id')->toArray();
        $division = Division::pluck('name','id')->toArray();


        return view('jobs.create')->with([
            'job' => $job,
            'offices' => $office,
            'divisions' => $division,
            'status' => $request->status,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->validationRules);
        $job = new Job;
        $job->fill($request->all());
        $job->annual_basic_salary = str_replace(',', '', $job->annual_basic_salary);
        if (!empty($job->publish)) {
            $job->publish = 1;
        }
        $job->created_by = \Auth::id();
        $job->save();

        return redirect()
            ->route('jobs.edit', [
                'id' => $job->id,
                'status' => $job->status
            ])
            ->with('success', 'The job post was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $job = Job::findOrFail($id);

        return view('jobs.show', compact('job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $job = Job::findOrFail($id);
        $office = Office::pluck('name','id')->toArray();
        $division = Division::pluck('name','id')->toArray();

        return view('jobs.edit', [
            'job' => $job,
            'status' => $request->status,
            'offices' => $office,
            'divisions' => $division
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->validationRules);

        $job = Job::findOrFail($id);
        $job->fill($request->all());
        $job->annual_basic_salary = str_replace(',', '', $job->annual_basic_salary);
        $job->publish = (!empty($request->publish)) ? 1 : 0;

        $job->update();

        return redirect()
            ->route('jobs.edit', [
                'id' => $job->id,
                'status' => $job->status
            ])
            ->with('success', 'The job post was successfully updated.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id job id value
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish(Request $request)
    {
        $job = Job::findOrFail($request->id);
        $job->publish = (empty($job->publish)) ? 1 : 0;
        $job->update();

        return \response()->json([
                'status' => 'success',
                'data' => [
                    'id' => $job->id
                ],
            ]
            , 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Job::destroy($id);

        return redirect('jobs')->with('success', 'Job post deleted!');
    }
}
