<?php

namespace App\Http\Controllers;

use App\Recommendation;
use App\PreliminaryEvaluation;
use App\Job;
use App\Applicant;
use App\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
class SelectedApplicantController extends Controller
{
     /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Selection Line Up');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $selected = Recommendation::latest()
            ->paginate($perPage);

        return view('selected_applicant.index', [
            'selectedData' => $selected,
            'action' => 'SelectedApplicantController@storeAppointee'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $jobs = Job::where('status', '=', 'plantilla')
            ->pluck('title', 'id')
            ->toArray();

        $recommend = Recommendation::select('applicant_id')->get()->toArray();
        $applicants = Applicant::where('qualified',1)->whereNotIn('id',$recommend)->getModels();

        return view('selected_applicant.create')->with([
            'action' => 'SelectedApplicantController@store',
            'actionQualified' => 'SelectedApplicantController@selectedApplicant',
            'jobs' => $jobs,
            'applicants' => $applicants,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        foreach ($request->recommend as $key => $recommendData) {

            if($recommendData['checked']){
                $recommend = new Recommendation;
                $recommend->applicant_id = $recommendData['applicant_id'];
                $recommend->created_by = Auth::id();
                $recommend->save();
            }
        }

        return redirect('/selected_applicant')->with('success','Selected applicant was saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SelectedApplicant  $selectedApplicant
     * @return \Illuminate\Http\Response
     */
    public function show(SelectedApplicant $selectedApplicant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SelectedApplicant  $selectedApplicant
     * @return \Illuminate\Http\Response
     */
    public function edit(SelectedApplicant $selectedApplicant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SelectedApplicant  $selectedApplicant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SelectedApplicant $selectedApplicant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SelectedApplicant  $selectedApplicant
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Recommendation::destroy($id);
        return redirect('/selected_applicant')->with('success','Selected applicant was deleted successfully.');
    }

    public function selectedApplicant(Request $request){
        $preliminary = new PreliminaryEvaluation;
        $preliminary = $preliminary->select('applicant_id')->get()->toArray();

        $currentJob = new Job();
        $applicant  = new Applicant;
        $recommend  = new Recommendation;
        $applicants = [];
        if (!empty($request->all())) {
            $currentJob = Job::find($request->position_consideration);
            $recommend = $recommend->pluck('applicant_id')->toArray();
            $applicants = $applicant
                ->whereNotIn('id',$recommend)
                ->where('job_id',$request->position_consideration)
                ->where('qualified',1)->getModels();
        }

        $jobs = Job::where('status', '=', 'plantilla')
            ->pluck('title', 'id')
            ->toArray();

        return view('selected_applicant.create')->with([
            'action' => 'SelectedApplicantController@store',
            'actionQualified' => 'SelectedApplicantController@selectedApplicant',
            'applicants' => $applicants,
            'currentJob' => $currentJob,
            'jobs' => $jobs,
        ]);
    }

    public function storeAppointee(Request $request)
    {
        $appointment = Appointment::where('applicant_id',$request->applicant_id)->first();
        if (empty($appointment)) {
            $appointment = new Appointment;
        }
        if($appointment->exists()){
            $appointment->updated_by = Auth::id();
            $response = redirect('/selected_applicant')->with('success', 'Applicant appointed was updated successfully.');
        }else{
            $appointment->created_by = Auth::id();

            $response = redirect('/selected_applicant')->with('success', 'Applicant appointed was created successfully.');
        }
        $appointment->applicant_id = $request->applicant_id;
        $appointment->save();

        return $response;
    }
}
