<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Assumption;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AssumptionController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Assumption');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $assumptions = Assumption::with([
                'applicant.job' => function ($query) {
                    $query->where('status', '=', 'plantilla');
                }
            ]
        )->paginate($perPage);

        return view('assumption.index', [
            'assumptions' => $assumptions
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Assumption $assumption
     * @return \Illuminate\Http\Response
     */
    public function show(Assumption $assumption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobOffer $jobOffer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $assumption = new Assumption();
        $applicant = new Applicant();

        if ($id) {
            $assumption = Assumption::find($id)->first();
        }

        return view('assumption.edit')->with([
            'assumption' => $assumption,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Assumption $assumption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $assumption = Assumption::find($id);
        $assumption->fill($request->all());
        $assumption->updated_by = Auth::id();

        $assumption->save();

       return redirect('/assumption')->with('success', 'Assumption was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Assumption $assumption
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Assumption::destroy($id);
        return redirect('/assumption')->with('success', 'Assumption was successfully deleted.');
    }

    public function assumptionReport(Request $request){
        return view('assumption.report');
    }
}
