<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Recommendation;
use App\Appointment;
use App\Evaluation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class RecommendationController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Recommendation');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $recommendations = Recommendation::latest()
            ->paginate($perPage);

        return view('recommendation.index', [
            'recommendations' => $recommendations,
            'action' => 'RecommendationController@store',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $recommendation = new Recommendation();
        $applicant = new Applicant();

        if ($request->reference) {
            $applicant = Applicant::where('reference_no', $request->reference)
                ->first();
        }

        return view('recommendation.create')->with([
            'recommendation' => $recommendation,
            'applicant' => $applicant,
            'action' => 'RecommendationController@store',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $appointment = Appointment::where('applicant_id',$request->applicant_id)->first();
        if (empty($appointment)) {
            $appointment = new Appointment;
        }
        if($appointment->exists()){
            $appointment->updated_by = Auth::id();
            $response = redirect('/recommendation')->with('success', 'Applicant appointed was updated successfully.');
        }else{
            $appointment->created_by = Auth::id();

            $response = redirect('/recommendation')->with('success', 'Applicant appointed was created successfully.');
        }
        $appointment->applicant_id = $request->applicant_id;
        $appointment->save();

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = explode('-', $id);

        $Id = $data[0];
        $applicant_id = $data[1];

        Evaluation::where('applicant_id',$applicant_id)
                 ->update(['recommended' => 0]);

        Recommendation::destroy($Id);
        return redirect('/recommendation')->with('success', 'Recommendation data deleted!');
    }
}
