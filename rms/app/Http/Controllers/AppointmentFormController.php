<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Appointment;
use App\AppointmentForm;
use App\Job;
use App\JobOffer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AppointmentFormController extends Controller
{
    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Appointment Form');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = Appointment::latest()
            ->paginate($perPage);

        return view('appointment-form.index', [
            'appointments' => $appointments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $applicant       = new Applicant;
        $appointmentform = new AppointmentForm;
        if (isset($request->applicant_id)) {
            $applicant = $applicant->where('id', $request->applicant_id)
                ->first();
            $appointmentform = $appointmentform->where('applicant_id',$request->applicant_id)->first();
        }

        return view('appointment-form.create')->with([
            'applicant' => $applicant,
            'appointmentform' => $appointmentform,
            'action' => 'AppointmentFormController@store',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $appointmentform = AppointmentForm::find($request->appointmentform_id);
        if(empty($appointmentform)){
            $appointmentform = new AppointmentForm;
        }
        $appointmentform->fill($request->all());
        if($appointmentform->exists()){
            $appointmentform->updated_by = Auth::id();
            $response = 'The Appointment was successfully updated.';
        }else{
            $appointmentform->created_by = Auth::id();
            $response = 'The Appointment was successfully created.';
        }
        $appointmentform->save();
        return redirect('/appointment-form')->with('success', $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AppointmentForm::destroy($id);
        return redirect('/appointment-form')->with('success', 'Appointment data deleted!');
    }
}
