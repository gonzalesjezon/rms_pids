<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'evaluations';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'applicant_id',
        'performance',
        'performance_divide',
        'performance_average',
        'performance_percent',
        'performance_score',
        'eligibility',
        'training',
        'seminar',
        'minimum_education_points',
        'minimum_training_points',
        'education_points',
        'training_points',
        'education_training_total_points',
        'education_training_percent',
        'education_training_score',
        'relevant_positions_held',
        'minimum_experience_requirement',
        'additional_points',
        'experience_accomplishments_total_points',
        'experience_accomplishments_percent',
        'experience_accomplishments_score',
        'potential',
        'potential_average_rating',
        'potential_percentage_rating',
        'potential_percent',
        'potential_score',
        'psychosocial',
        'psychosocial_average_rating',
        'psychosocial_percentage_rating',
        'psychosocial_percent',
        'psychosocial_score',
        'evaluated_by',
        'total_percent',
        'total_score',
        'reviewed_by',
        'noted_by',
        'created_by',
        'updated_by',
        'recommended',
    ];

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Applicant');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authorUpdate()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }
}
