<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOffer extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'job_offers';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [

        'applicant_id',
        'pera_amount',
        'clothing_allowance',
        'year_end_bonus',
        'cash_gift',
        'executive_director',
        'job_offer_date'
    ];

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Applicant');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authorUpdate()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }
}
