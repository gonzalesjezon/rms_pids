<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_status',
        'office_id',
        'division_id',
        'plantilla_item_number',
        'title',
        'grade',
        'monthly_basic_salary',
        'annual_basic_salary',
        'daily_salary',
        'status',
        'description',
        'education',
        'experience',
        'training',
        'eligibility',
        'requirements',
        'duties_responsibilities',
        'key_competencies',
        'expires',
        'publish',
        'pera_amount',
        'clothing_amount',
        'midyear_amount',
        'yearend_amount',
        'cashgift_amount',
        'compentency_1',
        'compentency_2',
        'compentency_3',
        'deadline_date',
    ];

    /**
     * Relation: Job has many applicants
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicants()
    {
        return $this->hasMany('App\Applicant');
    }

    public function evaluations()
    {
        return $this->hasMany('App\Evaluation');
    }

    public function office(){
        return $this->belongsTo('App\Office');
    }

    public function division(){
        return $this->belongsTo('App\Division');
    }
}
