@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
        rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" />
@endsection

{!! Form::open(['action' => $actionQualified, 'method' => 'GET', 'id' => 'evaluation-form']) !!}
<div class="form-group row">
  {{ Form::label('position_consideration', 'Position for Consideration', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    {{ Form::select('position_consideration', $jobs, '', [
            'id' => 'position_consideration',
            'class' => 'form-control form-control-xs',
            'placeholder' => 'Vacant Positions',
            'name'=>'position_consideration',
            'required' => true,
        ])
    }}
  </div>

  {{ Form::label('division_office', 'Division/Office', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    <input
      size="16"
      type="text"
      class="form-control form-control-sm"
      placeholder="Division/Office"
      value="{{@$currentJob->division->name}}"
      readonly
    >
  </div>
</div>

<div class="form-group row">
  {{ Form::label('education', 'Education', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-8">
    <div id="education"></div>
  </div>
</div>

<div class="form-group row">
  {{ Form::label('experience', 'Experience', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-8">
    <div id="experience"></div>
  </div>
</div>

<div class="form-group row">
  {{ Form::label('training', 'Training', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-8">
    <div id="training"></div>
  </div>
</div>

<div class="form-group row">
  {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-8">
    <div id="eligibility"></div>
  </div>
</div>
{!! Form::close() !!}

<div class="form-group row">
  <div class="col-12">
    {!! Form::open(['action' => $action, 'method' => $method, 'id' => 'matrix-form']) !!}
    <table id="table1" class="table table-striped table-hover table-fw-widget table-bordered">
      <thead>
      <tr class="text-center">
        <th>Name of Applicant Current Position</th>
        <th>Age</th>
        <th>Educational Background</th>
        <th>Work Experience</th>
        <th>Eligibility</th>
        <th>Training</th>
        <th>Remarks</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
      @foreach(@$applicants as $key => $applicant)
      <tr>
        <td>{{@$applicant->getFullName()}}
        </td>
        <td>
          <input
            size="2"
            type="text"
            value="{{ Carbon\Carbon::today()->diffInYears(@$applicant->birthday) }}"
            name="preliminary[{{$key}}][age]"
            class="form-control form-control-sm"
          >
        </td>
        <td>
          <textarea name="preliminary[{{$key}}][education]" id="education" cols="19" rows="5">Bachelor of Science in Computer Science</textarea>
        </td>
        <td>
          <textarea name="preliminary[{{$key}}][experience]" id="experience" cols="19" rows="5">10 Years Government, 2 Years Private</textarea>
        </td>
        <td>
          <textarea name="preliminary[{{$key}}][eligibility]" id="eligibility" cols="19" rows="5">Latest Civil Service Eligibility</textarea>
        </td>
        <td>
          <textarea name="preliminary[{{$key}}][training]" id="training" cols="19" rows="5">Latest Training 1, <br> Latest Training 2</textarea>
        </td>
        <td>
          <textarea name="preliminary[{{$key}}][remarks]" id="remarks" cols="19" rows="5" placeholder="Write your remarks here."></textarea>
        </td>
        <td>
          <div class="col-12 col-sm-8 col-lg-6 pt-1">
              <div class="switch-button switch-button-success switch-button-yesno">
                  <input type="checkbox" name="preliminary[{{$key}}][checked]" id="swt{{$key}}"><span>
                  <label for="swt{{$key}}"></label></span>
              </div>
          </div>
        </td>
        <input type="hidden" name="preliminary[{{$key}}][applicant_id]" value="{{$applicant->id}}">
      </tr>
      @endforeach
      </tbody>
    </table>
  </div>
</div>

<div class="form-group row text-center">
  <div class="col-12 col-sm-8 col-lg-3">
    <input
      size="16"
      type="text"
      value=""
      name="isc_chairperson"
      class="form-control form-control-sm"
    >
    <hr>
    ISC Chairperson
  </div>
  <div class="col-12 col-sm-8 col-lg-3">
    <input
      size="16"
      type="text"
      value=""
      name="isc_member_one"
      class="form-control form-control-sm"
    >
    <hr>
    ISC Member
  </div>
  <div class="col-12 col-sm-8 col-lg-3">
    <input
      size="16"
      type="text"
      value=""
      name="isc_member_two"
      class="form-control form-control-sm"
    >
    <hr>
    ISC Member
  </div>
  <div class="col-12 col-sm-8 col-lg-3">
    <input
      size="16"
      type="text"
      value=""
      name="ea_representative"
      class="form-control form-control-sm"
    >
    <hr>
    EA Representative
  </div>
</div>

<div class="form-group row text-right">
  <div class="col col-sm-12">
    <input type="hidden" name="status" value="matrix">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>

{!! Form::close() !!}

@section('scripts')
  <!-- JS Libraries -->
  <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <!-- wysiwyg -->
  <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();
    });

    // instansiate summernote(wysiwyg) editor
    const $education = $('#education');
    const $experience = $('#experience');
    const $eligibility = $('#eligibility');
    const $training = $('#training');

    // place data into summernote(wysiwyg) editor
    $education.summernote('code', `{!! @$currentJob->education !!}`);
    $experience.summernote('code', `{!! @$currentJob->experience !!}`);
    $eligibility.summernote('code', `{!! @$currentJob->eligibility !!}`);
    $training.summernote('code', `{!! @$currentJob->training !!}`);

    // remove tools on summernote(wysiwyg) editor
    $('.note-toolbar').remove();

    $('#position_consideration').change(function() {
      $('#evaluation-form').submit();
    });
  </script>
@endsection
