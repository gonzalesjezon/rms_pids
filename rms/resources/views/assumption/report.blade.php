@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-6">
    <div class="col-sm-3">CS Form No. 4 <br> Revised 2017</div>
    <div class="col-sm-6"></div>
  </div>

  <div class="row mb-8">
  	<div class="col-sm-12 text-center">
  		<h4 class="font-weight-bold">Republic of the Philippines</h4>
  		<h4 class="font-weight-bold">(Name of Agency)</h4>
  		<h3 class="font-weight-bold">CERTIFICATION OF ASSUMPTION TO DUTY</h3>
  	</div>
  </div>

  <div class="row mb-8">
  	<div class="col-sm-12">
  		<p class="text-justify" style="text-indent: 50px;">
  			This is to certify that Ms/Mr.___________________ has assumed
  			the duties and responsibilities as __________________________ of
  			________________________________ effective ______________________
  		</p>

  		<p class="text-justify" style="text-indent: 50px;">
  			This certification is issued in connection with the issuance of the appointment of Ms/Mr. ____________ as ______________
  		</p>

  		<p class="text-justify" style="text-indent: 50px;">
  			Done this _____ day of _________________ in ____________
  		</p>

  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-6"></div>
  	<div class="col-sm-3"></div>
  	<div class="col-sm-3 border-top"></div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-6"></div>
  	<div class="col-sm-3"></div>
  	<div class="col-sm-3 text-center">Head Office/Department/Unit</div>
  </div>

  <div class="row mb-8">
  	<div class="col-sm-1 text-right">Date </div>
  	<div class="col-sm-3 border-bottom"></div>
  </div>

  <div class="row mb-8">
  	<div class="col-sm-2 text-left">Attested By </div>
  </div>


  <div class="row mb-1">
  	<div class="col-sm-3 border-top"></div>
  </div>

  <div class="row mb-8">
  	<div class="col-sm-3 text-center">Highest Ranking HRMO</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-6" style="font-size: 11px;">
  		201 File <br>
  		Admin <br>
  		COA <br>
  		CSC
  	</div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-10"></div>
  	<div class="col-sm-2 border border-dark font-weight-bold">
  		<i>
  			For submission to CSCFO
  		with 30 days from the
  		date of assumption of the appointee
  		</i>
  	</div>
  </div>


</div>

  <div class="form-group row text-right" class="not-include">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection