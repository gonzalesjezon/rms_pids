@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Applicant Details</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('applicant.index' ) }}">
                        Applicants ( {{ $applicant->first_name }} {{ $applicant->last_name }} )</a></li>
            </ol>
        </nav>
    </div>

    <!-- Applicant Form -->
    @if(!$documentView)
        <div class="row">
            <div class="col">
                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header">
                        <div class="btn-group mb-4" role="group" aria-label="Basic example">
                            <a href="{{ route('dashboard') }}" class="btn btn-success"><i
                                        class="icon icon-left mdi mdi-home"></i> Dashboard</a>
                            <a href="{{ route('applicant.index') }}" class="btn btn-success"><i
                                        class="icon icon-left mdi mdi-view-list-alt"></i> Applicants</a>
                            <a href="{{ route('applicant.create') }}" class="btn btn-success"><i
                                        class="icon icon-left mdi mdi-account-add"></i> Add an applicant</a>
                        </div>
                        <div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle"
                                    aria-expanded="false">
                                <i class="icon icon-left mdi mdi-settings-square"></i> Options <span
                                        class="icon-dropdown mdi mdi-chevron-down"></span>
                            </button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start"
                                 style="position: absolute; transform: translate3d(0px, 30px, 0px); top: 0px; left: 0px; will-change: transform;">
                                <a href="{{ route('applicant.edit', $applicant->id) }}" class="dropdown-item"><i
                                            class="icon icon-left mdi mdi-edit"></i>Edit this applicant</a>
                                <div class="dropdown-divider"></div>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['applicant', $applicant->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete this applicant', array(
                                    'type' => 'submit',
                                    'style' => 'color: #504e4e',
                                    'class' => 'dropdown-item',
                                    'title' => 'Delete Job Post',
                                    'onclick'=>'return confirm("Confirm delete?")'
                                ))!!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="user-info-list card">
            <div class="card-header card-header-divider">
                {{ $applicant->getFullName() }}
                <span class="card-subtitle">
                Applied For: {{ $applicant->job->title }} / Applied On: {{ $applicant->created_at }}
                </span>
            </div>
            <div class="card-body">
                <table class="no-border no-strip skills">
                    <tbody class="no-border-x no-border-y">
                    <tr>
                        <td class="icon"><span class="mdi mdi-case"></span></td>
                        <td class="item">Position Applied For:</td>
                        <td>{{ $applicant->job->title }}</td>
                    </tr>
                    <tr>
                        <td class="icon"><span class="mdi mdi-collection-item-2"></span></td>
                        <td class="item">Application Reference No.</td>
                        <td>{{ $applicant->reference_no }}</td>
                    </tr>
                    <tr>
                        <td class="icon"><span class="mdi mdi-email"></span></td>
                        <td class="item">Email</td>
                        <td>{{ $applicant->email_address }}</td>
                    </tr>
                    <tr>
                        <td class="icon"><span class="mdi mdi-alert-polygon"></span></td>
                        <td class="item">Status</td>
                        <td>{{ $applicant->job->status }}</td>
                    </tr>
                    <tr>
                        <td class="icon"><span class="mdi mdi-phone"></span></td>
                        <td class="item">Mobile</td>
                        <td>{{ $applicant->mobile_number }}</td>
                    </tr>
                    <tr>
                        <td class="icon"><span class="mdi mdi-phone"></span></td>
                        <td class="item">Profile Picture</td>
                        <td>
                            @if($applicant->image_path)
                                <img src="{{ asset('storage/images/' . $applicant->image_path) }}"
                                     class="img-thumbnail rounded float-left" width="200" height="200"
                                     alt="profile picture">
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card card-contrast">
                    <div class="card-header card-header-contrast">
                        Applicant Personal Details
                        <span class="card-subtitle">Detailed information regarding applicant</span>
                    </div>
                    <div class="card-body">
                        <table class="no-border no-strip skills">
                            <tbody class="no-border-x no-border-y">
                            <tr>
                                <td>Birthday</td>
                                <td> :</td>
                                <td>{{ $applicant->birthday }}</td>
                            </tr>
                            <tr>
                                <td>Birth Place</td>
                                <td> :</td>
                                <td>{{ $applicant->birth_place }}</td>
                            </tr>
                            <tr>
                                <td>Gender</td>
                                <td> :</td>
                                <td>{{ $applicant->gender }}</td>
                            </tr>
                            <tr>
                                <td>Civil Status</td>
                                <td> :</td>
                                <td>{{ $applicant->civil_status }}</td>
                            </tr>
                            <tr>
                                <td>Citizenship</td>
                                <td> :</td>
                                <td>{{ $applicant->citizenOf->name }}</td>
                            </tr>
                            <tr>
                                <td>Filipino</td>
                                <td> :</td>
                                <td>{{ $applicant->filipino ? 'yes' : 'no' }}</td>
                            </tr>
                            <tr>
                                <td>Naturalized</td>
                                <td> :</td>
                                <td>{{ $applicant->naturalized ? 'yes' : 'no' }}</td>
                            </tr>
                            <tr>
                                <td>Height</td>
                                <td> :</td>
                                <td>{{ $applicant->height }}</td>
                            </tr>
                            <tr>
                                <td>Weight</td>
                                <td> :</td>
                                <td>{{ $applicant->weight }}</td>
                            </tr>
                            <tr>
                                <td>Blood Type</td>
                                <td> :</td>
                                <td>{{ $applicant->blood_type }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card card-contrast">
                    <div class="card-header card-header-contrast">
                        Address Information
                        <span class="card-subtitle">All job requirements are listed below</span>
                    </div>
                    <div class="card-body">
                        <table class="no-border no-strip skills">
                            <tbody class="no-border-x no-border-y">
                            <tr>
                                <td>House Number</td>
                                <td> :</td>
                                <td>{{ $applicant->house_number }}</td>
                            </tr>
                            <tr>
                                <td>Street</td>
                                <td> :</td>
                                <td>{{ $applicant->street }}</td>
                            </tr>
                            <tr>
                                <td>Subdivision</td>
                                <td> :</td>
                                <td>{{ $applicant->subdivision }}</td>
                            </tr>
                            <tr>
                                <td>Barangay</td>
                                <td> :</td>
                                <td>{{ $applicant->barangay }}</td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td> :</td>
                                <td>{{ $applicant->city }}</td>
                            </tr>
                            <tr>
                                <td>Province</td>
                                <td> :</td>
                                <td>{{ $applicant->province }}</td>
                            </tr>
                            <tr>
                                <td>Country</td>
                                <td> :</td>
                                <td>{{ $applicant->countryOf->name }}</td>
                            </tr>
                            <tr>
                                <td>Remarks</td>
                                <td> :</td>
                                <td>{{ $applicant->remarks }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card card-contrast">
                    <div class="card-header card-header-contrast">
                        Government ID's
                        <span class="card-subtitle">Government Related Details</span>
                    </div>
                    <div class="card-body">
                        <table class="no-border no-strip skills">
                            <tbody class="no-border-x no-border-y">
                            <tr>
                                <td>Pagibig</td>
                                <td> :</td>
                                <td>{{ $applicant->pagibig }}</td>
                            </tr>
                            <tr>
                                <td>GSIS Number</td>
                                <td> :</td>
                                <td>{{ $applicant->gsis }}</td>
                            </tr>
                            <tr>
                                <td>Philhealth</td>
                                <td> :</td>
                                <td>{{ $applicant->philhealth }}</td>
                            </tr>
                            <tr>
                                <td>Tin Number</td>
                                <td> :</td>
                                <td>{{ $applicant->tin }}</td>
                            </tr>
                            <tr>
                                <td>SSS Number</td>
                                <td> :</td>
                                <td>{{ $applicant->sss }}</td>
                            </tr>
                            <tr>
                                <td>Government Issue ID</td>
                                <td> :</td>
                                <td>{{ $applicant->govt_issued_id }}</td>
                            </tr>
                            <tr>
                                <td>Government Issued ID Number</td>
                                <td> :</td>
                                <td>{{ $applicant->govt_id_issued_number }}</td>
                            </tr>
                            <tr>
                                <td>Government ID place of issuance</td>
                                <td> :</td>
                                <td>{{ $applicant->govt_id_issued_place }}</td>
                            </tr>
                            <tr>
                                <td>Government ID Date Issued</td>
                                <td> :</td>
                                <td>{{ $applicant->govt_id_date_issued }}</td>
                            </tr>
                            <tr>
                                <td>Government ID Valid Until</td>
                                <td> :</td>
                                <td>{{ $applicant->govt_id_valid_until }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card card-contrast">
                    <div class="card-header card-header-contrast">
                        Qualification
                        <span class="card-subtitle">Details</span>
                    </div>
                    <div class="card-body">
                        <table class="no-border no-strip skills">
                            <tbody class="no-border-x no-border-y">
                            <tr>
                                <td>Education</td>
                                <td> :</td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>Work Experience</td>
                                <td> :</td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>Training</td>
                                <td> :</td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>Eligibility</td>
                                <td> :</td>
                                <td>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <div class="card card-contrast">
                <div class="card-header card-header-contrast">
                    Attached Documents
                    <span class="card-subtitle">All applicant documents attached below</span>
                </div>
                <div class="card-body">

                    @if($applicant->application_letter_path)
                        <div class="form-group row">
                            <div class="col-2 mt-1"><span class="badge badge-success">Application Letter</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->application_letter_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->application_letter_path) }}" class="badge badge-success" download>
                                    <span class="icon mdi mdi-download"> Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($applicant->pds_path)
                        <div class="form-group row">
                            <div class="col-2 mt-1"><span class="badge badge-success">PDS Document</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->application_letter_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->pds_path) }}" download  >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($applicant->employment_certificate_path)
                        <div class="form-group row">
                            <div class="col-2 mt-1"><span class="badge badge-success">Employment Certificate</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->application_letter_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->employment_certificate_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($applicant->tor_path)
                        <div class="form-group row">
                            <div class="col-2 mt-1"><span class="badge badge-success">Transcript of Records</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->application_letter_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->tor_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($applicant->coe_path)
                        <div class="form-group row">
                            <div class="col-2 mt-1"><span class="badge badge-success">Certificate of Eligibility</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->application_letter_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->coe_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($applicant->training_certificate_path)
                        <div class="form-group row">
                            <div class="col-2 mt-1"><span class="badge badge-success">Training Certificate</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->application_letter_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->training_certificate_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //initialize the javascript
            App.init();
        });
    </script>
@endsection
