{{--PRIMARY--}}
<div class="form-group row font-weight-bold" style="font-size: 10px;">
    <div class="col-2 text-center">INCLUSIVE DATES (mm/dd/yyyy)</div>
    <div class="col-2 text-center">POSITION TITLE</div>
    <div class="col-3 text-center">DEPARTMENT/AGENCY/OFFICE/COMPANY</div>
    <div class="col-1 text-center">MONTHLY SALARY</div>
    <div class="col-1 text-center">SALARY/JOB/SALARY GRADE</div>
    <div class="col-2 text-center">STATUS OF APPOINTMENT</div>
    <div class="col-1 text-left">GOV'T SERVICE (Y/N)</div>
</div>

<div class="row">
  <div class="col-12 text-left">
    <a href="#" id="add_workexperience" class="btn btn-sm btn-info">Add</a>
  </div>
</div>

<?php
    $experience_ctr     = 0;
    $experience_ctr2    = count($applicant->workexperience);
?>

@if($experience_ctr2 > 0)

<div class="row text-center mt-2" style="font-size: 10px;">
    <div class="col-2">
        <div class="row">
            <div class="col-6">From</div>
            <div class="col-6">To</div>
        </div>
    </div>
    <div class="col-2 pl-0 pr-0">(Write in full/Do not abbreviate)</div>
    <div class="col-3 pl-0 pr-0">(Write in full/Do not abbreviate)</div>
    <div class="col-1"></div>
    <div class="col-1"></div>
    <div class="col-2"></div>
    <div class="col-1"></div>
</div>

@foreach($applicant->workexperience as $key => $value)
<?php $experience_ctr += 1; ?>

<input type="hidden" name="work_experience[{{$key}}][id]" value="{{$value->id}}">
<div class="row {{ ($experience_ctr2 == $experience_ctr) ? 'work_experience' : '' }} mt-2">
    <div class="col-2 text-center">
        <div class="row">
            <div class="col-6 pr-1">
                <input type="text" name="work_experience[{{$key}}][inclusive_date_from]" class="form-control form-control-sm" value="{{$value->inclusive_date_from}}">

                {!! $errors->first('work_experience[$key][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
            <div class="col-6 pl-1">
                <input type="text" name="work_experience[{{$key}}][inclusive_date_to]" class="form-control form-control-sm" value="{{$value->inclusive_date_to}}">

                {!! $errors->first('work_experience[$key][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
    <div class="col-2 pr-1 pl-0 text-center font-weight-bold">
        <input type="text" name="work_experience[{{$key}}][position_title]" class="form-control form-control-sm" value="{{$value->position_title}}">

        {!! $errors->first('work_experience[$key][position_title]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-3 pr-0 pl-1 text-center font-weight-bold">
        <input type="text" name="work_experience[{{$key}}][department]" class="form-control form-control-sm" value="{{$value->department}}">

        {!! $errors->first('work_experience[$key][department]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1 text-center">
        <input type="text" name="work_experience[{{$key}}][monthly_salary]" class="form-control form-control-sm" value="{{$value->monthly_salary}}">

        {!! $errors->first('work_experience[$key][monthly_salary]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1  text-center">
        <input type="text" name="work_experience[{{$key}}][salary_grade]" class="form-control form-control-sm" value="{{$value->salary_grade}}">

        {!! $errors->first('work_experience[$key][salary_grade]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-2 pl-1 pr-1  text-center">
        <input type="text" name="work_experience[{{$key}}][status_of_appointment]" class="form-control form-control-sm" value="{{$value->status_of_appointment}}">

        {!! $errors->first('work_experience[$key][status_of_appointment]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1 text-center">
        <input type="text" name="work_experience[{{$key}}][govt_service]" class="form-control form-control-sm col-6 pr-0" value="{{$value->govt_service}}" style="display: inline-block;">

        <a  class="btn btn-danger col-5 remove" data-id="{{$value->id}}" data-level="experience"><i class="icon mdi mdi-delete" style="color:#fff !important;"></i></a>

        {!! $errors->first('work_experience[$key][govt_service]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}

    </div>
</div>
@endforeach

<input type="hidden" id="experience" value="{{$experience_ctr}}">
@else
<div class="form-group row work_experience">
    <div class="col-2 text-center">
        <div class="row">
            <div class="col-6 pr-1">
                <span style="font-size: 10px;">FROM</span>
                {{ Form::text('work_experience[1][inclusive_date_from]', '', [
                        'class' => 'form-control form-control-sm',
                    ])
                }}
                {!! $errors->first('work_experience[1][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
            <div class="col-6 pl-1">
                <span style="font-size: 10px;">TO</span>
                {{ Form::text('work_experience[1][inclusive_date_to]', '', [
                        'class' => 'form-control form-control-sm',
                    ])
                }}
                {!! $errors->first('work_experience[1][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
    <div class="col-2 pr-1 pl-0 text-center font-weight-bold">
        <span style="font-size: 10px;">(Write in full/Do not abbreviate)</span>
        {{ Form::text('work_experience[1][position_title]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][position_title]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-3 pr-0 pl-1 text-center font-weight-bold">
        <span style="font-size: 10px;">(Write in full/Do not abbreviate)</span>
        {{ Form::text('work_experience[1][department]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][department]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1 text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[1][monthly_salary]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][monthly_salary]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1  text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[1][salary_grade]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][salary_grade]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-2 pl-1 pr-1  text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[1][status_of_appointment]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][status_of_appointment]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1  text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[1][govt_service]', '', [
                'class' => 'form-control form-control-sm col-6',
            ])
        }}
        {!! $errors->first('work_experience[1][govt_service]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>
@endif


<div class="form-group row text-right">
    <div class="col-12">
        {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
        {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
    </div>
</div>