@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" />
@endsection

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Applicants</h2>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-table">
                    <div class="card-header">
                        <a href="{{ route('applicant.create') }}" class="btn btn-space btn-primary"
                           title="Add a vacant position">
                            <i class="icon icon-left mdi mdi-account-add"></i> Add
                        </a>
                        <a href="{{ route('applicant.index') }}" class="btn btn-space btn-warning" title="Print">
                            <i class="icon icon-left mdi mdi-account-add"></i> Print
                        </a>
                    </div>
                    <div class="card-body">
                        <table id="table1" class="table table-striped table-hover table-fw-widget">
                            <thead>
                            <tr class="text-center">
                                <th>Actions</th>
                                <th>Reference No.</th>
                                <th>Applicants Name</th>
                                <th>Position <br> Applied</th>
                                <th>Date <br> Applied</th>
                                <th>GWA</th>
                                <th>Status</th>
                                <th>Requirements</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($applicants as $applicant)
                                <tr class="text-center">
                                    <td class="actions text-left">
                                        <div class="tools">
                                            <button type="button" data-toggle="dropdown"
                                                    class="btn btn-secondary dropdown-toggle" aria-expanded="false">
                                                <i class="icon icon-left mdi mdi-settings-square"></i> Options
                                                <span class="icon-dropdown mdi mdi-chevron-down"></span>
                                            </button>
                                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                                <a href="{{ route('applicant.show',['id'=>$applicant->id] ) }}"
                                                   class="dropdown-item"><i class="icon icon-left mdi mdi-eye"></i>View</a>
                                                <div class="dropdown-divider"></div>

                                                <a href="{{ route('applicant.edit', $applicant->id) }}"
                                                   class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>Edit</a>
                                                <div class="dropdown-divider"></div>

                                                {!! Form::open([
                                                        'method' => 'POST',
                                                        'action' => 'ApplicantController@storeQualified',
                                                        'style'  => 'display:inline'
                                                ])!!}

                                                 {!! Form::button('<i class="icon icon-left mdi mdi-graduation-cap"></i>  Qualified',[
                                                        'type' => 'submit',
                                                        'style' => 'color: #504e4e',
                                                        'class' => 'dropdown-item',
                                                        'title' => 'Qualify',
                                                        'onclick'=>'return confirm("Confirm ?")'
                                                ])!!}
                                                <input type="hidden" name="applicant_id" id="applicant_id" value="{{ $applicant->id }}">
                                                {!! Form::close() !!}
                                                <div class="dropdown-divider"></div>

                                                <!--
                                                <a href="{{ route('evaluation.create', ['reference' => $applicant->reference_no]) }}"
                                                   class="dropdown-item"><i
                                                            class="icon icon-left mdi mdi-graduation-cap"></i>Evaluate</a>
                                                            <div class="dropdown-divider"></div>
                                                -->

                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['applicant', $applicant->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                {!! Form::button('<i class="icon icon-left mdi mdi-delete mr-1"></i>  Delete', array(
                                                    'type' => 'submit',
                                                    'style' => 'color: #504e4e',
                                                    'class' => 'dropdown-item',
                                                    'title' => 'Delete Job Post',
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                                ))!!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{ $applicant->reference_no }}</td>
                                    <td>{{ $applicant->first_name }} {{ $applicant->last_name }}</td>
                                    <td>{{ $applicant->job->title }}</td>
                                    <td>{{ $applicant->created_at->diffForHumans() }}
                                        / {{ $applicant->created_at->format('Y-m-d')}}
                                    </td>
                                    <td>{{ $applicant->gwa}}</td>
                                    <td>{{ ($applicant->qualified == 1) ? 'Qualified' : ''}}</td>
                                    <td>
                                        <a href="{{ route('applicant.show',['id'=>$applicant->id, 'document'=>true] ) }}"
                                           class="btn btn-secondary">
                                            View Documents</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('applicant._index-script')
@endsection
