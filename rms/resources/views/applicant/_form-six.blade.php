{{--PRIMARY--}}
<div class="form-group row font-weight-bold">
  <div class="col-3 text-center">CAREER SERVICE
  </div>
  <div class="col-2 text-center">RATING <br> (If Applicable)</div>
  <div class="col-2 text-center">DATE OF EXAMINATION/ <br> CONFERMENT</div>
  <div class="col-2 text-center">PLACE <br> OF EXAMINATION/ <br> CONFERMENT</div>
  <div class="col-1"></div>
  <div class="col-2 text-left">LICENSE <br> (If Applicable)</div>
</div>

<div class="row">
  <div class="col-12 text-left">
    <label id="add_eligibility" class="btn btn-sm btn-info col-form-label">Add</label>
  </div>
</div>

<?php
  $eligibility_ctr  = 0;
  $eligibility_ctr2 = count($applicant->eligibility);
?>

@if(count($applicant->eligibility) > 0)

@foreach($applicant->eligibility as $key => $value)

<?php $eligibility_ctr += 1; ?>

<div class="row {{ ($eligibility_ctr2 == $eligibility_ctr) ? 'eligibility' : '' }}">
  <input type="hidden" name="eligibility[{{$key}}][id]" value="{{$value->id}}">
  <div class="col-3 mt-4">
    {{ Form::select('eligibility[1][eligibility_ref]', config('params.eligibility_type'), $value->eligibility_ref, [
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Select Eligibility',
                'required' => true,
            ])
        }}
    {!! $errors->first('eligibility[1][eligibility_ref]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 mt-4">
    <input type="text" name="eligibility[{{$key}}][rating]" class="form-control form-control-sm" value="{{$value->rating}}">
    {!! $errors->first('eligibility[$key][rating]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center font-weight-bold mt-4">
    <input type="text" name="eligibility[{{$key}}][exam_date]" class="form-control form-control-sm" value="{{$value->exam_date}}">
    {!! $errors->first('eligibility[$key][exam_date]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center font-weight-bold mt-4">
    <input type="text" name="eligibility[{{$key}}][exam_place]" class="form-control form-control-sm" value="{{$value->exam_place}}">
    {!! $errors->first('eligibility[$key][exam_place]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center">
    NUMBER
    <input type="text" name="eligibility[{{$key}}][license_number]" class="form-control form-control-sm" value="{{$value->license_number}}">
    {!! $errors->first('eligibility[$key][license_number]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-left">
    Date of Validity
    <input type="text" name="eligibility[{{$key}}][license_validity]" class="form-control form-control-sm col-8 pr-0 mr-1" value="{{$value->license_validity}}" style="display:inline-block">
    {!! $errors->first('eligibility[$key][license_validity]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    <a  class="btn btn-danger col-3 remove" data-id="{{$value->id}}" data-level="eligibility"><i class="icon mdi mdi-delete" style="color:#fff !important;"></i></a>
  </div>
</div>

@endforeach
<input type="hidden" id="eligibility_ctr" value="{{$eligibility_ctr}}">
@else
<div class="row eligibility">
  <input type="hidden" name="eligibility[0][applicant_id]" value="{{$applicant->id}}">
  <div class="col-3 mt-4">
    {{ Form::select('eligibility[0][eligibility_ref]', config('params.eligibility_type'), '', [
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Select Eligibility',
                'required' => true,
            ])
        }}
    {!! $errors->first('eligibility[0][eligibility_ref]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 mt-4">
    {{ Form::text('eligibility[0][rating]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[0][rating]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center font-weight-bold mt-4">
    {{ Form::text('eligibility[0][exam_date]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[0][exam_date]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center font-weight-bold mt-4">
    {{ Form::text('eligibility[0][exam_place]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[0][exam_place]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center">
    NUMBER
    {{ Form::text('eligibility[0][license_number]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[0][license_number]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-left">
    Date of Validity
    {{ Form::text('eligibility[0][license_validity]', '', [
            'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
        ])
    }}
    {!! $errors->first('eligibility[0][license_validity]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>
@endif

<div class="form-group row text-right">
  <div class="col-12">
    {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
    {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>