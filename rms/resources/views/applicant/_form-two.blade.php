<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Birth Date <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{ $applicant->birthday }}" name="birthday"
                   class="form-control form-control-sm"
                   placeholder="Birthday">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Place of Birth <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::text('birth_place', $applicant->birth_place, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Birthplace',
                'required' => true,
            ])
        }}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Sex <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::select('gender', $gender, $applicant->gender, [
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Sex',
                'required' => true,
            ])
        }}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Civil Status <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::select('civil_status', $civilStatus, $applicant->civil_status, [
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Civil Status',
                'required' => true,
        ])}}
    </div>
</div>

<!-- <div class="form-group row">
    {{ Form::label('citizenship', 'Citizenship', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::select('citizenship', $countries, $applicant->citizenship, [
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Citizenship',
                'required' => true,
            ])
        }}
    </div>
</div> -->

<!-- <div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Filipino</label>
    <div class="col-12 col-sm-8 col-lg-6 pt-1">
        <div class="switch-button switch-button-success switch-button-yesno">
            <input type="checkbox" name="filipino" id="swt1" {{ $applicant->filipino ? 'checked' : '' }}><span>
                                    <label for="swt1"></label></span>
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Naturalized</label>
    <div class="col-12 col-sm-8 col-lg-6 pt-1">
        <div class="switch-button switch-button-success switch-button-yesno">
            <input type="checkbox" name="naturalized" id="swt2" {{ $applicant->naturalized ? 'checked' : '' }}><span>
                                        <label for="swt2"></label></span>
        </div>
    </div>
</div> -->

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Height <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::text('height', $applicant->height, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'In cm.',
                'required' => true,
            ])
        }}
    </div>
</div>
<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Weight <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::text('weight', $applicant->weight, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'in kg.',
                'required' => true,
            ])
        }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label('blood_type', 'Blood Type', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::text('blood_type', $applicant->blood_type, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Blood Type',
                'required' => true,
            ])
        }}
    </div>
</div>
<!-- <div class="form-group row">
    {{ Form::label('pagibig', 'Pagibig', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::text('pagibig', $applicant->pagibig, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Pagibig Number',
            ])
        }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label('gsis', 'GSIS', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::text('gsis', $applicant->gsis, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'GSIS Number',
            ])
        }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label('philhealth', 'Philhealth', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::text('philhealth', $applicant->philhealth, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Philhealth Number',
            ])
        }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label('tin', 'Tin Number', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::text('tin', $applicant->tin, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Tin Number',
            ])
        }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label('sss', 'SSS Number', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::text('sss', $applicant->sss, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'SSS Number',
            ])
        }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label('govt_issued_id', 'Government Issue ID', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::text('govt_issued_id', $applicant->govt_issued_id, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Government Issued ID',
            ])
        }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label('govt_id_issued_number', 'Government Issued ID Number', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::text('govt_id_issued_number', $applicant->govt_id_issued_number, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Government ID Issued Number',
            ])
        }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label('govt_id_issued_place', 'Government ID place of issuance', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-3">
        {{ Form::text('govt_id_issued_place', $applicant->govt_id_issued_place, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Government ID place of issuance',
            ])
        }}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right"> Government ID Date Issued </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{$applicant->govt_id_date_issued}}"
                   name="govt_id_date_issued" class="form-control form-control-sm"
                   placeholder="Government ID Date Issued">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right"> Government ID Valid Until </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{$applicant->govt_id_valid_until}}" name="govt_id_valid_until"
                   class="form-control form-control-sm" placeholder="Government ID Valid Until">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>
 -->
<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
        {{ Form::button('Previous', ['class'=>'btn btn-secondary btn-space wizard-previous btn-form-one', 'data-wizard' => '#wizard1']) }}
        {{ Form::button('Next Step', ['class'=>'btn btn-primary btn-space wizard-next btn-form-two', 'data-wizard' => '#wizard1']) }}
        {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
    </div>
</div>