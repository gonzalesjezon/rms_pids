@section('css')
      <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}

<div class="form-group row">
    <div class="col-3">
        {{ Form::label('applicant', 'Appointment Checklists for Applicant', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    </div>
    <div class="col-5">
        {{ Form::label('applicant_name', $applicant->getFullName() ,['class'=>'col-12 col-sm-2 col-form-label text-sm-left font-weight-bold']) }}
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <table class="table table-bordered">
            <tbody>
                <tr class="text-center">
                    <td></td>
                    <td>CHECKLIST OF COMMON REQUIREMENTS</td>
                    <td>HRM</td>
                    <td>CSFO</td>
                </tr>
                <tr>
                    <td colspan="4" style="font-size: 12px;">Instructions: Put a check if the requirements are complete. If incomplete, use the space provided to indicate the name of appointee and the lacking requirement/s.</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td><b>APPOINTMENT FORMS</b> (CS Form No. 33-B, Revised 2017) <br> - Original CSC copy of appointment form</td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-0">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                    @if($appointment->form33_hrmo == 1)
                                    <input type="checkbox" name="form33_hrmo" id="swt0" checked="true"><span>
                                    @else
                                    <input type="checkbox" name="form33_hrmo" id="swt0"><span>
                                    @endif
                                  <label for="swt0"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="form33_cscfo" class="form-control form-control-xs">{{$appointment->form33_cscfo}}</textarea>
                    </td>
                </tr>
                <tr>
                <td>2</td>
                    <td><b>PLANTILLA OF CASUAL APPOINTMENT</b> (CSC Form No. 34-B or D) <br> - Original CSC copy </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-0">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                    @if($appointment->form34b_hrmo == 1)
                                    <input type="checkbox" name="form34b_hrmo" id="swt1" checked="true"><span>
                                    @else
                                    <input type="checkbox" name="form34b_hrmo" id="swt1"><span>
                                    @endif
                                  <label for="swt1"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="form34b_cscfo" class="form-control form-control-xs">{{$appointment->form34b_cscfo}}</textarea>
                    </td>
                </tr>
                <tr>
                <td>3</td>
                    <td><b>PERSONAL DATA SHEET</b> (CS Form No. 212, Revised 2017)  </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-0">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                    @if($appointment->form212_hrmo == 1)
                                    <input type="checkbox" name="form212_hrmo" id="swt2" checked="true"><span>
                                    @else
                                    <input type="checkbox" name="form212_hrmo" id="swt2"><span>
                                    @endif
                                  <label for="swt2"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="form212_cscfo" class="form-control form-control-xs">{{$appointment->form212_cscfo}}</textarea>
                    </td>
                </tr>
               <tr>
                <td>4</td>
                    <td><b>ORIGINAL COPY OF AUTHENTICATED CERTIFICATE OF ELIGIBILITY/ RATING/ LICENSE</b> <br> - Except if the eligibility has been previously authenticated in 2004 or onward and recorded   </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-0">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                    @if($appointment->eligibility_hrmo == 1)
                                    <input type="checkbox" name="eligibility_hrmo" id="swt3" checked="true"><span>
                                    @else
                                    <input type="checkbox" name="eligibility_hrmo" id="swt3"><span>
                                    @endif
                                  <label for="swt3"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="eligibility_cscfo" class="form-control form-control-xs">{{$appointment->eligibility_cscfo}}</textarea>
                    </td>
                </tr>
                <tr>
                <td>5</td>
                    <td><b>POSITION DESCRIPTION FORM</b> (DBM-CSC Form No. 1, Revised 2017) </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-0">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                    @if($appointment->form1_hrmo == 1)
                                    <input type="checkbox" name="form1_hrmo" id="swt4" checked="true"><span>
                                    @else
                                    <input type="checkbox" name="form1_hrmo" id="swt4"><span>
                                    @endif
                                  <label for="swt4"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="form1_cscfo" class="form-control form-control-xs">{{$appointment->form1_cscfo}}</textarea>
                    </td>
                </tr>
                <tr>
                <td>6</td>
                    <td><b>OATH OF OFFICE</b> (CS Form No. 32, Revised 2017)</td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-0">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                    @if($appointment->form32_hrmo == 1)
                                    <input type="checkbox" name="form32_hrmo" id="swt5" checked="true"><span>
                                    @else
                                    <input type="checkbox" name="form32_hrmo" id="swt5"><span>
                                    @endif
                                  <label for="swt5"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="form32_cscfo" class="form-control form-control-xs">{{$appointment->form32_cscfo}}</textarea>
                    </td>
                </tr>
                <tr>
                <td>7</td>
                    <td><b>CERTIFICATE OF ASSUMPTION TO DUTY</b> (CS Form No. 4) </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-0">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                    @if($appointment->form4_hrmo == 1)
                                    <input type="checkbox" name="form4_hrmo" id="swt6" checked="true"><span>
                                    @else
                                    <input type="checkbox" name="form4_hrmo" id="swt6"><span>
                                    @endif
                                  <label for="swt6"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="form4_cscfo" class="form-control form-control-xs">{{$appointment->form4_cscfo}}</textarea>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    <input type="hidden" name="appointment_id" value="{{$appointment->id}}">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
      });
    </script>
@endsection


