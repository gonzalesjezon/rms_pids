@extends('layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" />
@endsection

@section('content')
  <div class="page-head">
    <h2 class="page-head-title">Appointment Checklist</h2>
  </div>
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-table">
          <div class="card-header">
            <a href="{{ route('jobs.create',['status'=>'plantilla']) }}"
               class="btn btn-space btn-warning"
               title="Add a vacant position">
              <i class="icon icon-left mdi mdi-print"></i> Print
            </a>
            <!-- <div class="tools dropdown">
              <span class="icon mdi mdi-download"></span><a href="#" role="button" data-toggle="dropdown"
                                                            class="dropdown-toggle"><span
                  class="icon mdi mdi-more-vert"></span></a>
              <div role="menu" class="dropdown-menu">
                <a href="#" class="dropdown-item">Action</a>
                <a href="#" class="dropdown-item">Another action</a>
                <a href="#" class="dropdown-item">Something else here</a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">Separated link</a>
              </div>
            </div> -->
          </div>
          <div class="card-body">
            <table id="table1" class="table table-striped table-hover table-fw-widget">
              <thead>
              <tr>
                <th>Actions</th>
                <th>Applicants Ref#</th>
                <th>Applicants Name</th>
                <th>Position Title</th>
                <th>Plantilla Item No.</th>
                <th>Job Grade</th>
              </tr>
              </thead>
              <tbody>
              @foreach($appointments as $appointment)
                <tr>
                  <td class="actions text-left">
                    <div class="tools">
                      <button type="button" data-toggle="dropdown"
                              class="btn btn-secondary dropdown-toggle" aria-expanded="false">
                        <i class="icon icon-left mdi mdi-settings-square"></i> Options
                        <span class="icon-dropdown mdi mdi-chevron-down"></span>
                      </button>

                      <div role="menu" class="dropdown-menu" x-placement="bottom-start">

                        <a href="{{ route('appointment.create', ['applicant_id' => $appointment->applicant_id]) }}"
                           class="dropdown-item">
                          <i class="icon icon-left mdi mdi-edit"></i>Edit</a>
                        <div class="dropdown-divider"></div>


                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['appointment', $appointment->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="icon icon-left mdi mdi-delete mr-1"></i> Delete', [
                            'type' => 'submit',
                            'style' => 'color: #504e4e',
                            'class' => 'dropdown-item',
                            'title' => 'Delete Job Post',
                            'onclick'=>'return confirm("Confirm delete?")'
                        ])!!}
                        {!! Form::close() !!}
<!--                         <div class="dropdown-divider"></div>

                        <a href="{{route('appointment.report',[
                              'applicant_id' => $appointment->applicant_id
                            ])}}"
                             class="dropdown-item md-trigger"
                             data-modal="md-flipH"
                             target="_blank"
                          >
                            <i class="icon icon-left mdi mdi-print"> </i>Print
                          </a>
                          <div class="dropdown-divider"></div>

                          </a> -->
                      </div>
                    </div>
                  </td>
                  <td>{{ $appointment->applicant->reference_no }}</td>
                  <td>{{ $appointment->applicant->getFullname() }}</td>
                  <td>{{ $appointment->applicant->job->title }}</td>
                  <td>{{ $appointment->applicant->job->plantilla_item_number }}</td>
                  <td>{{ $appointment->applicant->job->grade }}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net/js/jquery.dataTables.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.dataTables();
    });
  </script>
@endsection
