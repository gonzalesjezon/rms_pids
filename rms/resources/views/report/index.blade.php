@extends('layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('content')
  <div class="page-head">
    <h2 class="page-head-title">Reports</h2>
  </div>
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-table">
          <div class="card-header">
          </div>
          <div class="card-body p-2">
            <div class="form-group row">
              {{ Form::label('reports_name', 'Reports Name', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('reports_name', $reports, '', [
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select report',
                        'required' => true,
                    ])
                }}
              </div>
            </div>

            <div class="form-group row d-none hide" id="select_position">
              {{ Form::label('select_job', 'Vacant Position', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('select_job', $jobs, '', [
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select position',
                        'required' => true,
                    ])
                }}
              </div>
            </div>

            <div class="form-group row d-none hide" id="select_applicant">
              {{ Form::label('applicant_name', 'Name', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('name', $applicants, '', [
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select applicant',
                        'required' => true,
                    ])
                }}
              </div>
            </div>

            <div class="form-group row">
              <label class="col-12 col-sm-2 col-form-label text-sm-right">Print Date </label>
              <div class="col-12 col-sm-8 col-md-5 col-lg-4 col-xl-3">
                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                  <input size="16" type="text" value="" name="print_date"
                         class="form-control form-control-sm" id="print_date">
                  <div class="input-group-append">
                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                  </div>
                </div>
              </div>
            </div>

<!--             <div class="form-group row">
              <label class="col-12 col-sm-3 col-md-1 col-form-label text-sm-right"> Sex </label>
              <div class="col-12 col-sm-7 col-md-1 col-lg-1 col-xl-1">
                <div class="icon-container">
                  <div class="icon"><span class="mdi mdi-female"></span></div><span class="icon-class"> </span>
                </div>
              </div>
              <div class="col-12 col-sm-7 col-md-1 col-lg-1 col-xl-1">
                <div class="icon-container">
                  <div class="icon"><span class="mdi mdi-male"></span></div><span class="icon-class"> </span>
                </div>
              </div>
            </div> -->

            <hr>

            <div class="form-group row">
              <div class="col-4 offset-6">
                <buton class="btn btn-secondary" id="preview">Preview</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();

      var reportName;
      $(document).on('change','#reports_name',function(){
          jobId  = '';
          appId  = '';
          printDate = '';
          reportName = $(this).find(':selected').val();

          switch(reportName){
            case 'selection_lineup':
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              break;
            case 'checklist':
              $('#select_applicant').removeClass('d-none');
              $('#select_position').addClass('d-none');
              break;
            case 'preliminary_evaluation':
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              break;
            default:
              $('.hide').addClass('d-none')
              break;
          }
      });

      var jobId;
      $(document).on('change','#select_job',function(){
          jobId = $(this).find(':selected').val();

      })

      var appId;
      $(document).on('change','#select_applicant',function(){
          appId = $(this).find(':selected').val();

      })

      var printDate;
      $(document).on('change','#print_date',function(){
        printDate = $(this).val();
      });

      $(document).on('click','#preview',function(){
          var id;
          var param;
          if(jobId){
            id = jobId;
          }else{
            id = appId;
          }

          param = (id) ? '?id='+id : '';
          date = (printDate) ? '?date='+printDate : '';

          href = window.location+'/'+reportName+param+date;
          window.open(href, '_blank');
      });

    });
  </script>
@endsection
