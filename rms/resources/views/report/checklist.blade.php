@extends('layouts.print')

@section('css')
@endsection

@section('content')

<div class="row">
	<div class="col-12 text-center">
		<h4 class="font-weight-bold pb-0">APPOINTMENT PROCESSING CHECKLISTS</h4>
		<span>(REGULATED/NON-ACCREDITED)</span>
	</div>
</div>

<!-- <div class="row">
	<div class="col-12">
		<table class="table table-bordered">
			<tr>
				<td class="p-1">Name</td>
				<td class="p-1">{{$applicant->getFullName()}}</td>
			</tr>
			<tr>
				<td class="p-1">Date of Birth</td>
				<td class="p-1">{{$applicant->birthday}}</td>
			</tr>
			<tr>
				<td class="p-1">Position Title</td>
				<td class="p-1"></td>
			</tr>
			<tr>
				<td class="p-1">Agency</td>
				<td class="p-1"></td>
			</tr>
			<tr>
				<td class="p-1">Annual Compensation</td>
				<td class="p-1"></td>
			</tr>
			<tr>
				<td class="p-1">Item Number</td>
				<td class="p-1"></td>
			</tr>
		</table>
	</div>
</div>

<div class="row">
    <div class="col-12">
    	<label class="font-weight-bold card-title">Qualification Standards</label>
        <table class="table table-bordered">
            <tbody>
                <tr class="text-center">
                    <td class="p-1">Criteria</td>
                    <td class="p-1">Requirements</td>
                    <td class="p-1">Appointee's Qualification <br> (Provide Specific Details)</td>
                    <td class="p-1">QS MET</td>
                    <td class="p-1">Remarks</td>
                </tr>
                <tr>
                    <td class="p-1">Education</td>
                    <td class="p-1">{{$applicant->job->education}}</td>
                    <td class="p-1">{{$appointment->educ_qualification}}</td>
                    <td class="text-center p-1" >
                        @if($appointment->educ_check == 1)
                        YES
                        @else
                        NO
                        @endif
                    </td>
                    <td class="p-1">{{$appointment->educ_remarks}}</td>
                </tr>
                <tr>
                    <td class="p-1">Experience</td>
                    <td class="p-1">{{$applicant->job->experience}}</td>
                    <td class="p-1">{{$appointment->exp_qualification}}</td>
                    <td class="text-center p-1" >
                        @if($appointment->exp_check == 1)
                        YES
                        @else
                        NO
                        @endif
                    </td>
                    <td class="p-1">{{$appointment->exp_remarks}}</td>
                </tr>
                <tr>
                    <td class="p-1">Training</td>
                    <td class="p-1">{{$applicant->job->training}}</td>
                    <td class="p-1">{{$appointment->training_qualification}}</td>
                    <td class="text-center p-1" >
                    	@if($appointment->training_check == 1)
                        YES
                        @else
                        NO
                        @endif
                    </td>
                    <td class="p-1">{{$appointment->training_remarks}}</td>
                </tr>
                <tr>
                    <td class="p-1">Eligibility</td>
                    <td class="p-1">{{$applicant->job->eligibility}}</td>
                    <td class="p-1">{{$appointment->eligibility_qualification}}</td>
                    <td class="text-center p-1" >
                    	@if($appointment->eligibility_check == 1)
                        YES
                        @else
                        NO
                        @endif
                    </td>
                    <td class="p-1">{{$appointment->eligibility_remarks}} </td>
                </tr>
                <tr>
                    <td class="p-1">Others if applicable <br> (e.g., Age, Term of Office)</td>
                    <td class="p-1"></td>
                    <td class="p-1">{{$appointment->other_qualification}}</td>
                    <td class="text-center p-1" >
                    	@if($appointment->other_check == 1)
                        YES
                        @else
                        NO
                        @endif
                    </td>
                    <td class="p-1">{{$appointment->other_remarks}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div> -->

<div class="row">
    <div class="col-12">
    	<label class="font-weight-bold card-title">Common Requirements for Regular Appointments</label>
        <table class="table table-bordered">
            <tbody>
                <tr class="text-center">
                    <td></td>
                    <td class="p-1">Requirements</td>
                    <td class="p-1">HRMO</td>
                    <td class="p-1">CSFO</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td class="p-1">
                        <b>APPOINTMENT FORMS</b> (CS Form No. 33-B, Revised 2017) <br> - Original CSC copy of appointment form
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->form33_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td class="p-1">
                       <b>PLANTILLA OF CASUAL APPOINTMENT</b> (CSC Form No. 34-B or D) <br> - Original CSC copy
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->form34b_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td class="p-1">
                       <b>PERSONAL DATA SHEET</b> (CS Form No. 212, Revised 2017)
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->form212_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td class="p-1">
                       <b>ORIGINAL COPY OF AUTHENTICATED CERTIFICATE OF ELIGIBILITY/ RATING/ LICENSE</b> <br> - Except if the eligibility has been previously authenticated in 2004 or onward and recorded
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->eligibility_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>5</td>
                    <td class="p-1">
                       <b>POSITION DESCRIPTION FORM</b> (DBM-CSC Form No. 1, Revised 2017)
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->form1_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>6</td>
                    <td class="p-1">
                      <b>OATH OF OFFICE</b> (CS Form No. 32, Revised 2017)
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->form32_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>7</td>
                    <td class="p-1">
                      <b>CERTIFICATE OF ASSUMPTION TO DUTY</b> (CS Form No. 4)
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->form4_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td>
                        <p style="font-size: 10px;">
                            This is to certify that I have checked the veracity, <br> authenticity and completeness of all the <br>requirements in support of the appointments attached herein.
                        </p>
                        <p  style="font-size: 10px;" class="text-center pt-5">
                            ANDREA S. AGCAOILI <br> <span class="border-top border-dark" > Highest Ranking HRMO </span>
                        </p>
                    </td>
                    <td>
                        <p style="font-size: 10px;" >
                            This is to certify that I have checked  all the <br>  requirements in support  of the appointments  <br>attached herein and found these <br> to be  [  ] complete /  [  ]   lacking.
                        </p>
                        <p  style="font-size: 10px;" class="text-center">
                            <br> <span class="border-top border-dark" >CSC FO Receiving Officer</span>
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<!-- <div class="row">
    <div class="col-12">
    	<label class="font-weight-bold card-title">Additional and Specific Cases</label>
        <table class="table table-bordered">
            <tbody>
                <tr class="text-center">
                    <td class="p-1">Requirements</td>
                    <td class="p-1">Detail/Compliance</td>
                </tr>
                <tr>
                    <td class="p-1">
                        Erasures/alterations on the appointment and other supporting documents (Changes duly initialed by authorized officials and accompanies by a communication authenticating changes made)
                    </td>
                    <td class="p-1">{{$appointment->ar_01}}</td>
                </tr>
                <tr>
                    <td class="p-1">Appointee with decided administrative/criminal case (certified true copy of decision rendered submitted)</td>
                    <td class="p-1">{{$appointment->ar_02}}</td>
                </tr>
                <tr>
                    <td class="p-1">Discrepancy in name/place of birth (Requirements and procedures as amended by CSC Resolution No,991907 dated August 27, 1999)</td>
                    <td class="p-1">{{$appointment->ar_03}}</td>
                </tr>
                <tr>
                    <td class="p-1">COMELEC Ban (Exemption from COMELEC)</td>
                    <td class="p-1">{{$appointment->ar_04}}</td>
                </tr>
                <tr>
                    <td class="p-1">
                        Non-Disciplinary Demotion
                        <ul>
                            <li>Certification of the Agency Head that demotion is not a result of an administrative case</li>
                            <li>Written consent by the employee interposing no object to the demotion</li>
                        </ul>
                    </td>
                    <td class="p-1">{{$appointment->ar_05}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div> -->

<!-- <div class="row">
    <div class="col-12">
    	<label class="font-weight-bold card-title">FOR CSCFO ACTION</label>
        <table class="table table-bordered">
            <tbody>
                <tr class="text-center">
                    <td class="p-1 text-left">
                    	<p class="mb-6">Agency</p>
                    	<p class="mb-4">Action</p>
                    </td>
                    <td class="p-1 text-left">
                    	<p class="mb-6">( ) Regulated</p>
                    	<p class="mb-3">( ) Approved</p>
                    	<p class="mb-3">( ) Disapproved</p>
                    </td>
                    <td class="p-1 text-left" style="width: 60em;">
                    	<p class="mb-3">
                    		( ) Appointment submitted to CSCFO within 30 calendar days from date of issuance.
                    	</p>
                    	<p>
                    		( ) Appointment submitted to CSCFO beyond 30 calendar days from date of issuance.
                    	</p>
                    	<p style="text-indent: 20px;" class="mb-3">Effective:______________</p>
                    	<p class="mb-3">Ground/s for Invalidation</p>
                    </td>
                </tr>
                <tr>
                	<td class="text-left p-1">
                		<p class="mb-8">Evaluated By:</p>
                	</td>
                	<td class="text-left p-1">
                		<p class="mb-8">Verified By:</p>
                	</td>
                	<td class="text-left p-1">
                		<p class="mb-8">Final Action By:</p>
                	</td>
                </tr>
                <tr>
                	<td class="text-left p-1">Date:</td>
                	<td class="text-left p-1">Date:</td>
                	<td class="text-left p-1">Date:</td>
                </tr>
            </tbody>
        </table>
    </div>
</div> -->


<div class="form-group row text-right d-print-none">
<div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
  {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
  {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection