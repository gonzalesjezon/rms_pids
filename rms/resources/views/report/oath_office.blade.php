@extends('layouts.print')

@section('css')
@endsection

@section('content')

<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-1">
    <div class="col-sm-3">CS Form No. 32 <br> Revised 2017</div>
    <div class="col-sm-6"></div>
  </div>

  <div class="row mb-6">
  	<div class="col-sm-12 text-center">
  		<h4 class="font-weight-bold">Republic of the Philippines</h4>
  		<h4 class="font-weight-bold">(Name of Agency)</h4>
  		<h3 class="font-weight-bold">OATH OF OFFICE</h3>
  	</div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-12">
  		<p class="text-justify" style="text-indent: 50px;">
  			I, _____________(Name of the Appointee)________________ of ______________(Address of the Appointee)__________________ having been appointed to the position of ______________(Position Title)____________ hereby solemnly swear, that I will faithfully discharge to the best of my ability, the duties of my present position and of all others that I may hereafter hold under the Republic of the Philippines; that I will bear true faith and allegiance to the same; that I will obey the laws, legal orders, and decrees promulgated by the duly constituted authorities of the Republic of the Philippines; and that I impose this obligation upon myself voluntarily, without mental reservation or purpose of evasion.
  		</p>
  		<p style="text-indent: 50px;">SO HELP ME GOD.</p>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-6"></div>
  	<div class="col-sm-3"></div>
  	<div class="col-sm-3 border-top"></div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-6"></div>
  	<div class="col-sm-3"></div>
  	<div class="col-sm-3 text-center">(Signature over Printed Name of the Appointee)</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-2 text-right">Government ID: </div>
  	<div class="col-sm-3 border-bottom"></div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-2 text-right">ID Number: </div>
  	<div class="col-sm-3 border-bottom"></div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-2 text-right">Date Issued: </div>
  	<div class="col-sm-3 border-bottom"></div>
  </div>

  <hr>

  <div class="row mb-8">
  	<div class="col-sm-12">
  		<p style="text-indent: 50px;">Subscribed and sworn to before me this _______ day of ___________________, 20___ in __________________________________, Philippines.</p>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-6"></div>
  	<div class="col-sm-3"></div>
  	<div class="col-sm-3 border-top"></div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-6"></div>
  	<div class="col-sm-3"></div>
  	<div class="col-sm-3 text-center">(Signature over Printed Name of the Appointing Officer/Authority/ Head of Office)</div>
  </div>

</div>

 <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection