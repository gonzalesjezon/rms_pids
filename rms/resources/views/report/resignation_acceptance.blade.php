@extends('layouts.print')

@section('css')
@endsection

@section('content')

<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-1">
    <div class="col-sm-3">CS Form No. 10 <br> Series of 2017</div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-12 text-center">
  		<h4 class="font-weight-bold">Republic of the Philippines</h4>
  		<h4 class="font-weight-bold">(Name of Agency)</h4>
  		<h3 class="font-weight-bold">ACCEPTANCE OF RESIGNATION</h3>
  	</div>
  </div>

  <div class="row mb-3">
  	<div class="col-sm-8"></div>
  	<div class="col-sm-4">
  		Date
  	</div>
  </div>

  <div class="row mb-2">
  	<div class="col-sm-12">
  		(Name of Employee) <br>
  		(Employee’s Address)
  	</div>
  </div>

  <div class="row mb-6">
  	<div class="col-sm-12">
  		<p>Sir/Madam:</p>
  		<p style="text-indent: 50px;">In reply to your letter dated (Date of the Letter of Resignation) tendering your resignation from the position of (Position Title) in (Name of Office), may I inform you that the same is hereby accepted to take effect on (Date of the Effectivity of Resignation).</p>
  		<p style="text-indent: 50px;">Your services while employed from this Office have been rated as ________________ , for your reference.</p>
  	</div>
  </div>

  <div class="row mb-6">
  	<div class="col-sm-7"></div>
  	<div class="col-sm-5">Very truly yours,</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-7"></div>
  	<div class="col-sm-3 border-top text-center"> Appointing Officer/Authority</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-3">Received by: </div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-1"></div>
  	<div class="col-sm-3 border-top"></div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-1"></div>
  	<div class="col-sm-3 text-center">Signature over Printed Name</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-1"></div>
  	<div class="col-sm-4">Date: </div>
  </div>

</div>

 <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection