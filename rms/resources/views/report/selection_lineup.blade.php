@extends('layouts.print')

@section('css')
<style type="text/css">
	@media print{
		@page{
			size:A4;
		}

		.font-style-pt8{
			font-size: 7pt;
			text-align: justify;
		}
		.font-style-pt9{
			font-size: 9pt;
		}
	}
	.font-style-pt8{
		font-size: 7pt;
		text-align: justify;
	}
	.font-style-pt9{
		font-size: 9pt;
	}
</style>
@endsection

@section('content')
<div class="reports" style="width: 960px;margin: auto;font-family: Arial, Helvetica, sans-serif;">
	<div class="row mb-2 font-style-pt9">
		NAME OF APPLICANTS AND THEIR QUALIFICATIONS
	</div>

	<div class="row mb-2 font-style-pt9">
		Position to be Filled: {{$jobs->title}}
	</div>

	<div class="row mb-1 font-style-pt9">
		MINIMUM QUALIFICATION
	</div>

	<div class="row mb- font-style-pt9">
		<div class="col-12">
			- {{ $jobs->education }}
		</div>
	</div>

	<div class="row mb-1 font-style-pt9">
		<div class="col-12">
			- {{ $jobs->training }}
		</div>
	</div>

	<div class="row mb-1 font-style-pt9">
		<div class="col-12">
			- {{ $jobs->experience }}
		</div>
	</div>

	<div class="row mb-1 font-style-pt9">
		<div class="col-12 ">
			- {{ $jobs->eligibility }}
		</div>
	</div>

	<div class="row mb-2">
		<div class="col-6 font-style-pt8">
			- <b>Core Compentencies</b> <br>
			{!! $jobs->compentency_1 !!}
		</div>
		<div class="col-6 font-style-pt8">
			- <b>Functional Compentencies</b> <br>
			{!! $jobs->compentency_2 !!}
		</div>
	</div>

	@if($jobs->compentency_3)
	<div class="row mb-2">
		<div class="col-6 font-style-pt8">
			- <b>Leadership Compentencies</b> <br>
			{!! $jobs->compentency_3 !!}
		</div>
	</div>
	@endif

	<div class="row mb-2 font-style-pt9">
		<div class="col-12">
			<table class="table table-striped table-fw-widget table-bordered">
				<thead>
					<tr>
						<th>No.</th>
						<th>Name of Applicant</th>
						<th>GWA</th>
						<th>Education</th>
						<th>Work Experience</th>
						<th>Training</th>
						<th>Eligibility</th>
						<th>Remarks</th>
					</tr>
				</thead>
				<tbody>
					@foreach($recommend as $key => $value)
					<tr>
						<td class="text-center">{{ $value->id }}</td>
						<td>{{ $value->applicant->getFullName()}}</td>
						<td>{{ $value->gwa }}</td>
						<td>
							<ul>
								@foreach($value->applicant->education as $key => $educ)
								<li>{{ $educ->course }}</li>
								@endforeach
							</ul>
						</td>
						<td></td>
						<td>
							<ul>
								@foreach($value->applicant->training as $key => $el)
								<li>{{ $el->title_learning_programs }}</li>
								@endforeach
							</ul>
						</td>
						<td><ul>
								@foreach($value->applicant->eligibility as $key => $el)
								<li>{{ $el->name }}</li>
								@endforeach
							</ul></td>
						<td></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

</div>


 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection