@extends('layouts.print')

@section('css')
@endsection

@section('content')

<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
	<div class="report1" style="border: 10px solid #b1acac;padding: 10px;">
	  <div class="row mb-1">
	    <div class="col-sm-3">CS Form No. 33-A <br> Revised 2017</div>
	    <div class="col-sm-6"></div>
	    <div class="col-sm-3 text-right">(Stamp of Date of Receipt)</div>
	  </div>

	  <div class="row mb-6">
	  	<div class="col-sm-12 text-center">
	  		<h4 class="font-weight-bold">Republic of the Philippines</h4>
	  		<h4 class="font-weight-bold">(Name of Agency)</h4>
	  	</div>
	  </div>

	  <div class="row mb-4">
	  	<div class="col-sm-12">
	  		<p>Mr./Mrs./ Ms.:   </p>
	  		<p style="text-indent: 60px;">You are hereby appointed as <span id="position">(Position Title)</span> (SG/JG/PG___) under (Employee Status) status at the (Office/Department/Unit) with compensation rate of (Salary in word) (P Salary in Number) pesos per month.</p>
	  		<p style="text-indent: 60px;">The nature of this appointment is (Original/Promotion, etc.) vice , who (Transferred, Retire, etc.) with Plantilla No.(Plantilla No) Page.</p>
	  		<p style="text-indent: 60px;">This appointment shall take effect on the date of signing by the appointing officer/authority.            </p>
	  	</div>
	  </div>

	  <div class="row mb-6">
	  	<div class="col-sm-7"></div>
	  	<div class="col-sm-5">Very truly yours,</div>
	  </div>

	  <div class="row mb-6">
	  	<div class="col-sm-7"></div>
	  	<div class="col-sm-3 border-top text-center"> Appointing Officer/Authority</div>
	  </div>

	  <div class="row mb-1">
	  	<div class="col-sm-7"></div>
	  	<div class="col-sm-3 border-top text-center"> Date of Signing</div>
	  </div>
	</div>

	<br>

	<div class="report1" style="border: 10px solid #b1acac;padding: 10px;">
		<div class="row mb-8">
			<div class="col-sm-12 font-weight-bold">CSC ACTION:</div>
		</div>

		<div class="row mb-1">
			<div class="col-1"></div>
			<div class="col-sm-3 border-top"></div>
		</div>
		<div class="row mb-8">
			<div class="col-1"></div>
			<div class="col-sm-3 text-center font-weight-bold">Authorize Official</div>
		</div>

		<div class="row mb-1">
			<div class="col-1"></div>
			<div class="col-sm-3 border-top"></div>
		</div>
		<div class="row mb-5">
			<div class="col-1"></div>
			<div class="col-sm-3 text-center font-weight-bold">Date</div>
		</div>
		<div class="row mb-2">
			<div class="col-sm-12 text-right"><i>(Stamp of Date of Release)</i></div>
		</div>
	</div>

	<br>

	<div class="report1" style="border: 10px solid #b1acac;padding: 14px;">
		<div class="row mb-2">
			<div class="col-sm-12 font-weight-bold text-center"><h4>Certification</h4></div>
		</div>

		<div class="row mb-4">
			<div class="col-sm-12">
				<p style="text-indent: 50px;" class="text-justify">This is to certify that all requirements and supporting papers pursuant to CSC MC No. ________have been complied with, reviewed and found to be in order.</p>
				<p style="text-indent: 50px;" class="text-justify">
					The position was published at ___________________________ from ___________ to _________,
					20_____ and posted in _____________________________________ from___________ to__________, 20_____ in consonance with RA No. 7041. The assessment by the Human Resource Merit Promotion and Selection Board (HRMPSB) started on ______________, 20_____.
				</p>
			</div>
		</div>

		<div class="row mb-6">
		  	<div class="col-sm-7"></div>
		  	<div class="col-sm-3 border-top text-center"> Highest Ranking HRMO</div>
		  </div>
	</div>

	<div class="report1" style="border: 10px solid #b1acac;padding: 14px;">
		<div class="row mb-2">
			<div class="col-sm-12 font-weight-bold text-center"><h4>Certification</h4></div>
		</div>

		<div class="row mb-4">
			<div class="col-sm-12">
				<p style="text-indent: 50px;" class="text-justify">This      is     to       certify      that       the      appointee      has     been     screened     and     found     qualified by the majority of the HRMPSB during the deliberation held on __________________. </p>
			</div>
		</div>

		<div class="row mb-6">
		  	<div class="col-sm-7"></div>
		  	<div class="col-sm-3 border-top text-center"> Chairperson, HRMPSB</div>
		  </div>
	</div>
	<div style="margin-top: 20em;"></div>
	<div class="report1" style="border: 10px solid #b1acac;padding: 14px;">
		<div class="row mb-2">
			<div class="col-sm-12 font-weight-bold text-center"><h4>CSC Notation</h4></div>
		</div>

		<div class="row mb-6">
			<div class="col-sm-12 border-top"></div>
		</div>

		<div class="row mb-6">
			<div class="col-sm-12 border-top"></div>
		</div>

		<div class="row mb-6">
			<div class="col-sm-12 border-top"></div>
		</div>

		<div class="row mb-6">
			<div class="col-sm-12 border-top"></div>
		</div>

		<div class="row mb-6">
			<div class="col-sm-12 border-top"></div>
		</div>

	</div>
	<br>
	<div class="report1" style="border: 10px solid #b1acac;padding: 14px;">

		<div class="row mb-6"></div>
		<div class="row mb-4">
			<div class="col-sm-12">
				<p style="text-indent: 50px;" class="text-justify">ANY ERASURE OR ALTERATION ON THE CSC ACTION SHALL NULLIFY OR INVALIDATE THIS APPOINTMENT EXCEPT IF THE ALTERATION WAS AUTHORIZED BY THE COMMISSION. </p>
			</div>
		</div>
	</div>
	<br>
	<div class="report1" style="border: 10px solid #b1acac;padding: 14px;">

		<div class="row mb-1">
			<div class="col-sm-6">
				<div class="mb-6"></div>
				<ul style="list-style: none;">
					<li>Original Copy   -   for the Appointee</li>
					<li>Original Copy   -   for the Civil Service Commission</li>
					<li>Original Copy   -   for the Agency</li>
				</ul>
			</div>
			<div class="col-sm-6 text-center">
				<p class="font-weight-bold">Acknowledgement</p>
				<p>Received original/photocopy of appointment on_____________</p>
			</div>
		</div>

		<div class="row mb-6">
		  	<div class="col-sm-7"></div>
		  	<div class="col-sm-3 border-top text-center"> Highest Ranking HRMO</div>
		  </div>
	</div>

</div>

 <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection