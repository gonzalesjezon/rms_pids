@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'job-form']) !!}


<div class="row">
    <div class="col-6">
        <div class="form-group {{ $errors->has('employee_status') ? 'has-error' : ''}} row">
            {{ Form::label('employee_status', 'Employee Status', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('employee_status', config('params.employee_status'),$job->employee_status,[
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select employee status',
                        'required' => true,
                    ])
                }}
                {!! $errors->first('employee_status', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('title') ? 'has-error' : ''}}">
            {{ Form::label('title', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('title', $job->title, [
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                ])
                }}
                {!! $errors->first('title', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('plantilla_item_number') ? 'has-error' : ''}}">
            {{ Form::label('plantilla_item_number', 'Plantilla Item No', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('plantilla_item_number', $job->plantilla_item_number, [
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                ])
                }}
                {!! $errors->first('plantilla_item_number', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('office_id') ? 'has-error' : ''}}">
            {{ Form::label('office_id', 'Office', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('office_id', $offices, $job->office_id,[
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select office',
                    ])
                }}
                {!! $errors->first('office_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('division_id') ? 'has-error' : ''}}">
            {{ Form::label('division_id', 'Division', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('division_id', $divisions, $job->division_id,[
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select division',
                        'required' => true,
                    ])
                }}
                {!! $errors->first('division_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>

    <div class="col-6">
        @if($status === 'plantilla')
        <div class="form-group {{ $errors->has('grade') ? 'has-error' : ''}} row">
            {{ Form::label('grade', 'Job Grade', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('grade', $job->grade, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Job Grade',
                    'required' => true,
                ])
                }}
                {!! $errors->first('grade', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('monthly_basic_salary') ? 'has-error' : ''}}">
            {{ Form::label('monthly_basic_salary', 'Monthly Basic Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-xl-6 col-sm-8 col-lg-6">
                {{ Form::text('monthly_basic_salary', $job->monthly_basic_salary, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'PHP 0',
                    'required' => true,
                ])
                }}
                {!! $errors->first('annual_basic_salary', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('annual_basic_salary') ? 'has-error' : ''}}">
            {{ Form::label('annual_basic_salary', 'Annual Basic Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-xl-6 col-sm-8 col-lg-6">
                {{ Form::text('annual_basic_salary', $job->annual_basic_salary, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'PHP 0',
                    'required' => true
                ])
                }}
                {!! $errors->first('annual_basic_salary', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
        @endif

        @if($status == 'non-plantilla')
        <div class="form-group row {{ $errors->has('daily_salary') ? 'has-error' : ''}} {{ ($status == 'plantilla') ? 'd-none' : '' }}">
            {{ Form::label('daily_salary', 'Daily Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('daily_salary', $job->daily_salary, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Daily Salary',
                    'required' => false,
                ])
                }}
                {!! $errors->first('daily_salary', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    @endif
    </div>
</div>

<hr>

<div class="row">
    <div class="col-6">

        <div class="form-group row ">
            {{ Form::label('', 'Additional Allowance', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
        </div>

        <div class="form-group row {{ $errors->has('pera_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'PERA', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('pera_amount', $job->pera_amount,[
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                    'placeholder' => '0.00'
                ]) }}
            </div>
            {!! $errors->first('pera_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('clothing_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Clothing Allowance', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('clothing_amount', $job->clothing_amount,[
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                    'placeholder' => '0.00'
                ]) }}
            </div>
            {!! $errors->first('clothing_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('midyear_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Mid-Year Bonus', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('midyear_amount', $job->midyear_amount,[
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                    'placeholder' => '0.00'
                ]) }}
            </div>
            {!! $errors->first('midyear_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

    </div>


    <div class="col-6">

        <div class="form-group row {{ $errors->has('yearend_amount') ? 'has-error' : '' }} mt-8">
            {{ Form::label('', 'Year-End Bonus', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('yearend_amount', $job->yearend_amount,[
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                    'placeholder' => '0.00'
                ]) }}
            </div>
            {!! $errors->first('yearend_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('cashgift_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Cash Gift', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('cashgift_amount', $job->cashgift_amount,[
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                    'placeholder' => '0.00'
                ]) }}
            </div>
            {!! $errors->first('cashgift_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('description') ? 'has-error' : ''}}">
            {{ Form::label('description', 'Position Description', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-9">
                <div id="description" name="description"></div>
                <textarea name="description" class="d-none"></textarea>
                <input type="hidden" name="description" id="description-text">
                {!! $errors->first('description', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_1') ? 'has-error' : ''}}">
            {{ Form::label('', 'Core Compentencies', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="compentency_1" name="compentency_1"></div>
                {{ Form::textarea('compentency_1', '',['id'=>'compentency_1-text', 'class'=>'d-none']) }}
                {!! $errors->first('compentency_1', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

    </div>

    <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_2') ? 'has-error' : ''}}">
            {{ Form::label('', 'Functional Compentencies', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="compentency_2" name="compentency_2"></div>
                {{ Form::textarea('compentency_2', '',['id'=>'compentency_2-text', 'class'=>'d-none']) }}
                {!! $errors->first('compentency_2', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_3') ? 'has-error' : ''}}">
            {{ Form::label('', 'Leadership Compentencies', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="compentency_3" name="compentency_3"></div>
                {{ Form::textarea('compentency_3', '',['id'=>'compentency_3-text', 'class'=>'d-none']) }}
                {!! $errors->first('compentency_3', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('education') ? 'has-error' : ''}}">
            {{ Form::label('education', 'Education', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="education" name="education"></div>
                {{ Form::textarea('education', $job->education,['id'=>'education-text', 'class'=>'d-none']) }}
                {!! $errors->first('education', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('training') ? 'has-error' : ''}}">
            {{ Form::label('training', 'Training', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="training" name="training"></div>
                {{ Form::textarea('training', $job->training,['id'=>'training-text', 'class'=>'d-none']) }}
                {!! $errors->first('training', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row {{ $errors->has('experience') ? 'has-error' : ''}}">
            {{ Form::label('experience', 'Experience', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="experience" name="experience"></div>
                {{ Form::textarea('experience', $job->experience,['id'=>'experience-text', 'class'=>'d-none']) }}
                {!! $errors->first('experience', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('eligibility') ? 'has-error' : ''}}">
            {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="eligibility" name="eligibility"></div>
                {{ Form::textarea('eligibility', $job->eligibility,['id'=>'eligibility-text', 'class'=>'d-none']) }}
                {!! $errors->first('eligibility', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

@if($status == 'non-plantilla')
    <div class="form-group row {{ $errors->has('duties_responsibilities') ? 'has-error' : ''}} {{ ($status == 'plantilla') ? 'd-none' : '' }}">
        {{ Form::label('duties_responsibilities', 'Duties and Responsibilities', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-6">
            <div id="duties_responsibilities" name="duties_responsibilities"></div>
            {{ Form::textarea('duties_responsibilities', $job->duties_responsibilities,['id'=>'duties_responsibilities-text', 'class'=>'d-none']) }}
            {!! $errors->first('duties_responsibilities', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>

    <div class="form-group row {{ $errors->has('key_competencies') ? 'has-error' : ''}} {{ ($status == 'plantilla') ? 'd-none' : '' }}">
        {{ Form::label('key_competencies', 'Key Competencies', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-6">
            <div id="key_competencies" name="key_competencies"></div>
            {{ Form::textarea('key_competencies', $job->key_competencies,['id'=>'key_competencies-text', 'class'=>'d-none']) }}
            {!! $errors->first('key_competencies', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>

    <div class="form-group row {{ $errors->has('requirements') ? 'has-error' : ''}} {{ ($status == 'plantilla') ? 'd-none' : '' }}">
        {{ Form::label('requirements', 'Requirements', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-6">
            <div id="requirements" name="requirements"></div>
            {{ Form::textarea('requirements', $job->requirements,['id'=>'requirements-text', 'class'=>'d-none']) }}
            {!! $errors->first('requirements', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>
@endif

@if(false)
    <div class="form-group row {{ $errors->has('expires') ? 'has-error' : ''}}">
        {{ Form::label('expires', 'Expires', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{$job->expires}}
            <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                {{ Form::text('expires', $job->expires, [
                'class' => 'form-control',
                'placeholder' => 'Expires',
                'required' => true,
                ])
                }}
                <div class="input-group-append">
                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                </div>
            </div>
            {!! $errors->first('division', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>
@endif

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Publish</label>
    <div class="col-12 col-sm-8 col-lg-6 pt-1">
        <div class="switch-button switch-button-success switch-button-yesno">
            <input type="checkbox" name="publish" id="swt8" {{ $job->publish ? 'checked' : '' }}><span>
                                <label for="swt8"></label></span>
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right"> Deadline Date </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{ $job->deadline_date }}" name="deadline_date"
                   class="form-control form-control-sm">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    @include('jobs._form-script')
@endsection
