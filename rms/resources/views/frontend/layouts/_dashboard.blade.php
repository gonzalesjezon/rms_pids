<body>
	<img src="{{ url('img/header.jpg') }}" width="100%">
   @include('frontend.layouts._main-content')
   <script src="{{ URL::asset('beagle-assets/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
   <script src="{{ URL::asset('beagle-assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
   <script src="{{ URL::asset('beagle-assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
   <script src="{{ URL::asset('beagle-assets/js/app.js') }}" type="text/javascript"></script>
   @yield('scripts')
</body>
