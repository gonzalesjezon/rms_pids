@extends('frontend.layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" />
@endsection

@section('content')
    <div class="bg-light">
        <div class="main-content container-fluid bg-light">
          @include('layouts._flash-message')
            <div class="card card-table">
                <div class="card-header">
                  <h3 class="mb-4">JOB OPPORTUNITIES</h3>
                </div>
                <div class="card-body">
                    <table id="table1" class="table table-striped table-hover table-fw-widget">
                        <thead>
                            <tr class="text-center">
                                <th>Item Number / Position Title</th>
                                <th>Required Qualifications</th>
                                <th>Employment Notice</th>
                                <th>Deadline of Application</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($jobs as $key => $job)
                            <tr>
                                <td>
                                  {{$job->title}} <br> {{$job->plantilla_item_number}}
                                  <label> SG {{ $job->grade }}</label>
                                </td>
                                <td>
                                    <ul>
                                        <li>{{ $job->education}}</li>
                                        <li>{{ $job->experience}}</li>
                                        <li>{{ $job->training}}</li>
                                        <li>{{ $job->eligibility}}</li>
                                    </ul>
                                </td>
                                <td>
                                     <a href="{{ route('frontend.show',['id'=>$job->id] ) }}" class="btn btn-info {{ (isset($job->deadline_date)) ? 'mt-3' : '' }}" target="_blank"><i class="icon icon-left mdi mdi-eye"></i> View</a>
                                </td>
                                <td>
                                    <div class="text-center">
                                      <span style="font-size: 12px;">{{ $job->deadline_date }}</span>
                                    </div>
                                    <button data-modal="md-flipH" class="btn btn-info md-trigger show" data-job_id="{{$job->id}}"> Apply Now</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class=" form-group row">
              <div class="col-md-12">
                  <h4 class="font-weight-bold">Address your application letter to:</h4>
              </div>
            </div>

            <div class="form-group row mb-1">
              <div class="col-md-12">
                <p>
                  <span class="font-weight-bold">HRMO II</span> <br>
                  Administrative Division <br>
                  Philippine Institute for Development Studies <br>
                  18th Floor, Three Cyberpod Centris <br>
                  EDSA cor. Quezon Ave., Quezon City
                </p>
              </div>
            </div>

            <div class=" form-group row">
              <div class="col-md-12">
                  <h4 class="font-weight-bold">Attach the following: (INCOMPLETE Application shall be excluded in the screening)</h4>
              </div>
            </div>

            <div class="form-group row mb-1">
              <div class="col-md-12">
                <p>
                  Curriculum Vitae <br>
                  Transcript of Grades <br>
                  Personal Data Sheet <br>
                  copy of Civil Service Certificate of Eligibility <br>
                  Result of performance evaluation from the last rating period (if coming from another government agency) <br>
                  Information sheet <br>
                  Three (3) Character References
                </p>
              </div>
            </div>

        </div>
    </div>

@include('frontend._notification')
@endsection

@section('scripts')
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net/js/jquery.dataTables.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}"
            type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.dataTables();

      $.fn.niftyModal('setDefaults', {
          overlaySelector: '.modal-overlay',
          contentSelector: '.modal-content',
          closeSelector: '.modal-close',
          classAddAfterOpen: 'modal-show',
      });

      var jobId;
      $(document).on('click','.show',function(){
        jobId = $(this).data('job_id');
      });

      var check;
      $('input[name=i_agree]').on('change',function(){
        check = $(this).val();
      });

      $('#btn-submit').on('click',function(){

         if(check == 'yes'){
            href = window.location+'/create?id='+jobId;
            window.open(href, '_blank');
            $('.mdi-close').trigger('click');
         }else{
            return false;
         }

      })

    });
  </script>
@endsection
