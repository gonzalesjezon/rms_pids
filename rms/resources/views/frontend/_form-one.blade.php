<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Applied Position</label>
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('job_name', $jobs->title, [
                'class' => 'form-control form-control-sm',
                'readOnly' => true
            ])
        }}
        <input type="hidden" name="job_id" value="{{$jobs->id}}">
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">First Name <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('first_name', '', [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'First Name',
                'required' => true,
            ])
        }}
        {!! $errors->first('first_name', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('middle_name', 'Middle Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('middle_name', '', [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Middle Name',
            ])
        }}
        {!! $errors->first('middle_name', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Last Name <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('last_name', '', [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Last Name',
                'required' => true,
            ])
        }}
        {!! $errors->first('last_name', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('extension_name', 'Extension Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('extension_name', '', [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Extension Name',
            ])
        }}
        {!! $errors->first('extension_name', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('nickname', 'Nick Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('nickname', '', [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Nickname',
            ])
        }}
        {!! $errors->first('nickname', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Email Address <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('email_address', '', [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Email Address',
                'required' => true
            ])
        }}
        {!! $errors->first('email_address', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Mobile Number <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('mobile_number', '', [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Mobile Number',
                'required' => true,
            ])
        }}
        {!! $errors->first('mobile_number', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('telephone_number', 'Telephone Number', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('telephone_number', '', [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Telephone Number',
            ])
        }}
        {!! $errors->first('telephone_number', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Where did you find this vacant position? <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::select('publication', config('params.publication'), '', [
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Where did you find this vacant position?',
                'required' => true,
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('gwa', 'GWA', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('gwa', '', [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'GWA',
            ])
        }}
        {!! $errors->first('gwa', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
        {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
        {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
    </div>
</div>