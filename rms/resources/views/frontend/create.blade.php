@extends('frontend.layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Application Form</h2>
    </div>

    <!-- Applicant Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">IMPORTANT:
                        Please make sure that you will enter a working, valid email address. This is where the Confirmation Notice will be sent. PIDS-HR must be able to contact you through your email address and telephone numbers.
                    </span>
                </div>
                <div class="card-body">
                    @include('frontend._form', [
                        'action' => $action,
                        'method' => 'POST',
                    ])
                </div>
                <div class="card-footer">
                    Please fill-out all required fields, double-check for typographical errors, and make sure all the requirements are attached.
                    Also, click on the "Disclaimer" button and confirm that you have read and understood PIDS policy on handling your personal information. Once done, you may now submit your job application to PIDS HR.
                    Thank you.

                </div>
            </div>
        </div>
    </div>
@endsection
