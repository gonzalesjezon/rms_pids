<!-- Nifty Modal-->
<div id="md-flipH" class="full-width modal-container modal-effect-8">
    <div class="modal-content modal-certificate px-1">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span
                        class="mdi mdi-close"></span></button>
        </div>

        <div id="printThis" class="modal-body rounded">
            <div class="row mb-4">
                <div class="col text-center" style="color:#4285f4;"><h4><strong>OPPORTUNITIES IN PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES</strong></h4></div>
            </div>
            <hr>
            <div class="row mb-4">
                <div class="col-md-12">
                    <p style="text-indent: 20px;text-align: justify;">
                        By providing us your personal information relative to your job application with us, you are signifying your unequivocal consent to its collection and use by our Human Resource Division.
                    </p>
                    <p style="text-indent: 20px;text-align: justify;">
                        Our Human Resource Division is the sole authorized unit that will handle your personal information throughout the course of your application process and subsequent employment with us. However, if you fail the application process, your personal information will be removed from our data base within three (3) months.
                    </p>
                    <p style="text-indent: 20px;text-align: justify;">
                       Rest assured that we take online information security very seriously, and we use the latest structures and practices to safeguard against loss, misuse and alteration of the information you provide. If you have any questions on the privacy of your information you may inquire at e2esolutions@gmail.com.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center ">
                    <label class="form-check-label form-check-inline">
                        <input type="radio"  name="i_agree" class="custom-radio" value="yes" >&nbsp; I agree
                    </label>
                    <label class="form-check-label form-check-inline">
                        <input type="radio"  name="i_agree" class="custom-radio" value="no">&nbsp; I disagree
                    </label>
                </div>
            </div>

            <div class="row text-right">
                <div class="col-md-12">
                     <button class="btn btn-success btn-space" id="btn-submit">Submit</button>
                </div>
            </div>
        </div>
        <div class="modal-footer"></div>
    </div>
</div>