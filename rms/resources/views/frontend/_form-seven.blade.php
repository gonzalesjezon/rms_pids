{{--PRIMARY--}}
<div class="form-group row font-weight-bold" style="font-size: 10px;">
    <div class="col-2 text-center">INCLUSIVE DATES (mm/dd/yyyy)</div>
    <div class="col-2 text-center">POSITION TITLE</div>
    <div class="col-3 text-center">DEPARTMENT/AGENCY/OFFICE/COMPANY</div>
    <div class="col-1 text-center">MONTHLY SALARY</div>
    <div class="col-1 text-center">SALARY/JOB/SALARY GRADE</div>
    <div class="col-2 text-center">STATUS OF APPOINTMENT</div>
    <div class="col-1 text-left">GOV'T SERVICE (Y/N)</div>
</div>

<div class="row">
  <div class="col-12 text-left">
    <a href="#" id="add_workexperience" class="btn btn-sm btn-info">Add</a>
  </div>
</div>

<div class="form-group row work_experience">
    <div class="col-2 text-center">
        <div class="row">
            <div class="col-6 pr-1">
                <span style="font-size: 10px;">FROM</span>
                {{ Form::text('work_experience[1][inclusive_date_from]', '', [
                        'class' => 'form-control form-control-sm',
                    ])
                }}
                {!! $errors->first('work_experience[1][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
            <div class="col-6 pl-1">
                <span style="font-size: 10px;">TO</span>
                {{ Form::text('work_experience[1][inclusive_date_to]', '', [
                        'class' => 'form-control form-control-sm',
                    ])
                }}
                {!! $errors->first('work_experience[1][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
    <div class="col-2 pr-1 pl-0 text-center font-weight-bold">
        <span style="font-size: 10px;">(Write in full/Do not abbreviate)</span>
        {{ Form::text('work_experience[1][position_title]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][position_title]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-3 pr-0 pl-1 text-center font-weight-bold">
        <span style="font-size: 10px;">(Write in full/Do not abbreviate)</span>
        {{ Form::text('work_experience[1][department]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][department]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1 text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[1][monthly_salary]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][monthly_salary]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1  text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[1][salary_grade]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][salary_grade]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-2 pl-1 pr-1  text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[1][status_of_appointment]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][status_of_appointment]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1  text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[1][govt_service]', '', [
                'class' => 'form-control form-control-sm col-6',
            ])
        }}
        {!! $errors->first('work_experience[1][govt_service]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>


<div class="form-group row text-right">
    <div class="col-12">
        {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
        {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
    </div>
</div>