@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div class="reports" style="width: 760px;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
	<div class="row mb-5">
		<div class="col-sm-12 text-center">
			<h4 class="font-weight-bold mb-0">Philippine Institute for Development Studies</h4>
			<p class="mb-3">18th  Floor, Three Cyberpod Centris, EDSA cor. Quezon Ave., Quezon City</p>
			<h4 class="font-weight-bold">E M P L O Y M E N T   N O T I C E </h4>
		</div>
	</div>

	<div class="row mb-2">
		<div class="col-sm-12">
			<p style="text-indent: 5em;text-align: justify;" class="mb-5">The   Philippine   Institute   for   Development   Studies   (PIDS),   a   nonstock,   nonprofit government  corporation  engaged  in  the  conduct  of  long-term  policy-oriented  research,  is  now considering applicants for the following positions</p>

			<p class="font-weight-bold">{{ $jobs->title }}</p>

			<ul class="mb-5">
				<li>{{ $jobs->education}}</li>
				<li>{{ $jobs->experience}}</li>
				<li>{{ $jobs->training}}</li>
				<li>{{ $jobs->eligibility}}</li>
			</ul>

			@if($jobs->compentency_1)
			<i class="mb-3 font-weight-bold">Core Compentencies</i>
			<p class="mb-5">{!! $jobs->compentency_1 !!}</p>
			@endif

			@if($jobs->compentency_2)
			<i class="mb-3 font-weight-bold">Functional Compentencies</i>
			<p class="mb-5">{!! $jobs->compentency_2 !!}</p>
			@endif

			@if($jobs->compentency_3)
			<i class="mb-3 font-weight-bold">Leadership Compentencies</i>
			<p class="mb-5">{!! $jobs->compentency_3 !!}</p>
			@endif

			<p class="mb-5">
				The  position  is  Salary  Grade  (SG)  –  {{$jobs->grade}}  at  P  {{ number_format($jobs->monthly_basic_salary,2)}}  per  month  and  the  nature  of appointment is {{ config('params.employee_status.'.$jobs->employee_status)}}.
			</p>

			<p style="text-indent: 5em;text-align: justify;">
				Interested parties shall forward their curriculum vitae, transcript of grades, Personal Data Sheet, copy of Civil Service Certificate of Eligibility, result of performance evaluation from the last rating period (if coming from another government agency), information sheet, and three (3) character    references.    We    will    appreciate    if    applications    are    sent    thru    e-mail    at jluna@mail.pids.gov.ph.   Otherwise,   please   send   the   requirements   indicated   above   to   the following address not later than :
			</p>
		</div>
	</div>

</div>


@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection